package com.sy.controller;

import com.sy.dto.Result;
import com.sy.entity.Order;
import com.sy.entity.Product;
import com.sy.service.OrderService;
import com.sy.vo.CartProductParams;
import com.sy.vo.OrderItemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RestController
public class OrderController {
    @Autowired
   private OrderService orderService;
    @RequestMapping(value = "orders/items",method = RequestMethod.GET)
   public  Result getOrderItmes(@RequestParam(value = "oid") Integer oid, @RequestHeader(value = "token") String token){
       return orderService.getOrderItmes(oid,token);
    }

    @Autowired
    private HttpServletResponse res;

    @RequestMapping(value = "orders/order", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Order getOrderByid(@RequestParam(value = "oid") Integer oid){
        return orderService.getOrderByid(oid);
    }

    /**
     * @Description: 获取所有的订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 21:57
     */
    @RequestMapping(value = "getOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrders(){
        return orderService.getOrders();
    }

    /**
     * @Description: 通过订单编号和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrdersByCondition(@RequestParam("keyword") String keyword,@RequestParam("times")String times) {
        return orderService.getOrdersByCondition(keyword,times);
    }

    /**
     * @Description: 通过产品名称和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "refundOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrdersByConditions(@RequestParam("keyword") String keyword,@RequestParam("times")String times) {
        return orderService.getOrdersByConditions(keyword,times);
    }

    /**
     * @Description: 管理员发货操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "deliverGoods/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result deliverGoods(@PathVariable("id")Integer id) {
        return orderService.deliverGoods(id);
    }

    /**
     * @Description: 管理员退款操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "refund/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result refund(@RequestParam("id")@PathVariable("id")Integer id) {
        return orderService.refund(id);
    }

    /**
     * @Description: 删除订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "delOrder", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delOrder(String id) {
        return orderService.delOrder(id);
    }

    /**
     * 商品详情生成订单
     * @Author: 孙玮立
     * @param token
     * @param orderItemParams
     * @return
     */
    @RequestMapping(value = "orders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result generateOrder(@RequestHeader("auth-token")String token,@RequestBody List<OrderItemParam> orderItemParams){
        System.out.println(orderItemParams);
        Result result = orderService.generateOrder(token,orderItemParams);
        res.setStatus(result.getCode());
        return result;
    }

    /**
     * 购物车生成订单
     * @Author: 孙玮立
     * @param token
     * @param cartProductParams
     * @return
     */
    @RequestMapping(value = "orders/carts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result generateCartOrder(@RequestHeader("auth-token")String token,@RequestBody List<CartProductParams> cartProductParams){
        System.out.println(cartProductParams);
        Result result = orderService.generateCartOrder(token,cartProductParams);
        res.setStatus(result.getCode());
        return result ;
    }

    /**
     * @Description: 获取当前用户的订单积分获取记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/integrations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getIntegrationHistory(@RequestParam("id")Integer id){
        return orderService.getIntegrationHistory(id);
    }


    /**
     * @Description: 获取当前用户的订单浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/browses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getBrowseHistory(@RequestParam("id")Integer id){
        return orderService.getBrowseHistory(id);
    }


    /**
     * @Description: 获取当前用户的订单购物记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrderHistory(@RequestParam("id")Integer id){
        return orderService.getOrderHistory(id);
    }
}
