package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.OrderProductRef;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description:订单和商品具体信息的关系表
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface OrderProductRefMapper extends BaseMapper<OrderProductRef> {

    @Select(value = " SELECT * FROM order_info WHERE oid=#{oid}")
    List<OrderProductRef> getOrderProductRefs(@Param(value = "oid") Integer oid);
}
