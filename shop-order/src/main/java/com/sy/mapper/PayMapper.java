package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Order;
import com.sy.entity.Pay;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

public interface PayMapper extends BaseMapper<Pay> {
    @Select(value = "select *from pay")
    List<Pay> getPayMethods();
}
