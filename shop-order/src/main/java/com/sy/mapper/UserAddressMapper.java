package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.UserAddress;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

public interface UserAddressMapper  extends BaseMapper<UserAddress> {
    @Select(value = "select *from user_address WHERE uid=#{uid} AND  status=1")
     UserAddress getUserDefalutAddress(@Param(value = "uid") Integer uid);
}
