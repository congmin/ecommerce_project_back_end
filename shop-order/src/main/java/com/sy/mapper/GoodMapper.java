package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Category;
import com.sy.entity.Product;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 朱天宇 2020-3-11
 *
 * 商品相关的mapper
 *
 * */
@Repository
public interface GoodMapper extends BaseMapper<Category> {
    //这里的1代表的是父节点，0的话代表不是父节点！！！
    @Select("select *from category where isparent=1")
    List<Category> getParentNodes();
    //这里的parentid代表的是数据库内的parent字段
    @Select("select * from category where parent=#{parentid}")
    List<Category> getCatroys(@Param(value = "parentid") Integer parentid);
    @Select("select * from product where id=#{pid}")
    Product getProductById(@Param(value = "pid") Integer pid);
    @Select("select * from product")
    List<Product> getProducts();

}
