package com.sy.filter;


import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class TokenFilter extends ZuulFilter {
    @Override
    public String filterType() {
        //filterType方法，返回一个字符串代表过滤器的类型，在zuul中定义了四种不同生命周期的过滤器类型，具体如下：
        //pre：路由之前
        //routing：路由之时
        //post： 路由之后
        //error：发送错误调用
        return FilterConstants.PRE_TYPE;
    }


    @Override
    public int filterOrder() {
        // filterOrder：过滤的顺序,如果有多个过滤器，值越小，执行顺序越靠前
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        //shouldFilter：表示是否要执行过滤操作
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //run：过滤器的核心逻辑
        //RequestContext对象可以获取当前的请求对象和响应对象
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        //获取请求参数的值
//        request.getHeader();
        Object accessToken = request.getParameter("token");
        //如果在请求参数中不存在token
        if (accessToken == null) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            try {
                //则在页面中写出未登录的信息
                ctx.getResponse().setHeader("Content-Type", "text/html;charset=UTF-8");
                ctx.getResponse().getWriter().write("登录信息为空！");
            } catch (Exception e) {
            }
            return null;
        }
        return null;
    }
}
