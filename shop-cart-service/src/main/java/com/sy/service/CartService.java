package com.sy.service;

import com.sy.dto.Result;
import com.sy.vo.CartProductParams;
import com.sy.vo.OrderItemParam;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/11 16:23
 */


@FeignClient(value = "cart-module",contextId = "CartService")
public interface CartService {
    @RequestMapping(value = "carts",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    Result addToCart(@RequestParam("token") String token, @RequestParam("orderItemParam") OrderItemParam orderItemParam);

    @RequestMapping(value = "carts",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    Result getCartItems(@RequestParam("token")String token);

    @RequestMapping(value = "carts",method = RequestMethod.DELETE,produces = MediaType.APPLICATION_JSON_VALUE)
    Result clearSomeCart(@RequestParam("token")String token,@RequestParam("fields")String fields);

    @RequestMapping(value = "carts",method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    Result updateCartItem(@RequestParam("token")String token,@RequestParam("token")String pid, @RequestParam("count")String count);

    @RequestMapping(value = "carts/users",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    Result addCollections(@RequestParam("token")String token,@RequestParam("pids")String pids);
}
