package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description:
 * @Author: 陈聪敏
 * @Date: 2020/3/15 0015 19:43
 */
@Data
@TableName("brand")
public class Brand implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String brandno;
    private String logo;
    private String brandname;
    private String country;
    private String describes;
    private Date jointime;
    private Integer status;
}
