package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName(value = "product")
public class Product implements Serializable {
    private static final long serialVersionUID = 2131424342342343242L;
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String productno;
    private String productpicture;
    private String productname;
    private BigDecimal originalprice;
    private BigDecimal presentprice;
    private String country;
    private Date joiningtime;
    private String  auditstatus;
    private Integer status;
    private Integer clickcount;
    private Integer cid;
    private Integer bid;
    private String edible;
    @TableField("qualitytime")
    private String  qualityTime;
    private Integer volume;
    private Integer  stock;
    private Integer goodadvice;

}
