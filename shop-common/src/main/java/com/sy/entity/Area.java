package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库表对应的区实体类
 * @Author: 董星奇
 */
@Data
@TableName("area")
public class Area implements Serializable {
    private static final long serialVersionUID = -710547440993432642L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField("areaid")
    private Integer areaid;

    @TableField("area")
    private String area;

    @TableField("cityid")
    private Integer cityid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAreaid() {
        return areaid;
    }

    public void setAreaid(Integer areaid) {
        this.areaid = areaid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    @Override
    public String toString() {
        return "Area{" +
                "id=" + id +
                ", areaid=" + areaid +
                ", area='" + area + '\'' +
                ", cityid=" + cityid +
                '}';
    }
}
