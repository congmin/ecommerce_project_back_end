package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 数据库role表对应的实体类
 * @Author: 董星奇
 */
@Data
@TableName("role")
public class Role implements Serializable {
    private static final long serialVersionUID = -2466933746970276059L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField("role")
    private String role;

    @TableField("descs")
    private String descs;

}
