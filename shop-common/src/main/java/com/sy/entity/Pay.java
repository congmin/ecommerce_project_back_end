package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName(value = "pay")
public class Pay {
    private  Integer id;
    private String method;
    private Integer status;
    private String describe;
}
