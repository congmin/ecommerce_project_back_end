package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description: 文章的实体类
 * @Author: 陈聪敏
 * @Date: 2020/3/16 0016 21:11
 */
@Data
@TableName(value = "article")
public class Article implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer sorting;
    private String articleno;
    private String type;
    private String title;
    private String introduce;
    private Date addingtime;
    private Integer status;
    private Integer sid;
}
