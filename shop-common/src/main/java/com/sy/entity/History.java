package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/17 0:12
 */
@Data
@TableName(value = "history")
public class History implements Serializable {
    private static final long serialVersionUID = 3892810651448157541L;
    private Integer id;
    private Integer pid;
    private Integer uid;
    private Timestamp browsestime;
}
