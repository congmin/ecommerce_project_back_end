package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description: 管理员的实体类
 * @Author: 陈聪敏
 * @Date:
 */

@Data
@TableName(value = "admin")
public class Admin implements Serializable {
    private static final long serialVersionUID = -565799219880681877L;
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    @TableField(value = "username")
    private String userName;
    private String pwd;
    private Integer sex;
    private Integer age;
    private String phone;
    private String email;
    private String qq;
    private Date regtime;
    private Integer status;
}
