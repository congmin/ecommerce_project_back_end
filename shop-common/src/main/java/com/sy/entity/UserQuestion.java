package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库表用户安全问题关系对应实体类
 * @author 董星奇
 */
@Data
@TableName("user_question")
public class UserQuestion implements Serializable {
    private static final long serialVersionUID = 1004824056184769121L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("uid")
    private Integer uid;
    /**
     * 问题id
     */
    @TableField("sid")
    private Integer sid;

    @TableField("answer")
    private String answer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "UserQuestion{" +
                "id=" + id +
                ", uid=" + uid +
                ", sid=" + sid +
                ", answer='" + answer + '\'' +
                '}';
    }
}
