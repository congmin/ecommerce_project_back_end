package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库对应安全问题实体类
 * @author 董星奇
 */
@Data
@TableName("safequestion")
public class SafeQuestion implements Serializable {
    private static final long serialVersionUID = 3302006150592209413L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 问题
     */
    @TableField("question")
    private String question;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "SafeQuestion{" +
                "id=" + id +
                ", question='" + question + '\'' +
                '}';
    }
}
