package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 孙玮立
 * @Date:
 */
@Data
@TableName(value = "evaluate")
public class Evaluate implements Serializable {

    private static final long serialVersionUID = 3761722573932210561L;
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    private Integer uid;
    private String  evaluate;
    @TableField("evaluatetime")
    private String  evaluatetime;
    private String  type;

}
