package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/11 20:44
 */
@TableName("cart")
@Data
public class Cart implements Serializable {
    private static final long serialVersionUID = -8154759280904182234L;
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer pid;
    private Integer count;
    private Integer fid;
    private Integer packid;
}
