package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName("order_info")
public class OrderProductRef implements Serializable {
    private Integer oid;
    private Integer pid;
    private Integer count;
    private Integer fid;
    private Integer packid;
}
