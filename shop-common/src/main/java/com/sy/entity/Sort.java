package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description:
 * @Author: 陈聪敏
 * @Date: 2020/3/16 0016 21:54
 */
@Data
@TableName(value = "sort")
public class Sort implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer sorting;
    private String sortno;
    private String sortname;
    private String introduce;
    private Date addtime;
    private Integer status;
}
