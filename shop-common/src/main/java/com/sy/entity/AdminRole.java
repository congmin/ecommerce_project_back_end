package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 管理员和权限关联表的实体类
 * @Author: 陈聪敏
 * @Date:
 */
@Data
@TableName(value = "admin_role")
public class AdminRole implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    @TableField(value = "aid")
    private Integer userId;
    @TableField(value = "rid")
    private Integer roleId;
}
