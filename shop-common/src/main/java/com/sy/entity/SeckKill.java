package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 * pid:商品id
 * count:库存数量
 *
 */
@Data
@TableName(value = "activity")
public class SeckKill implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long pid;
    private Long count;
    private Integer time;
    private Integer isable;
    private Integer userid;
}
