package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName(value = "flavor")
public class Flavor implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    private String flavor;
}
