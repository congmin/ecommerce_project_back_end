package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户新增的收货地址实体类
 * @author 董星奇
 */
@Data
@TableName("address")
public class Address implements Serializable {
    private static final long serialVersionUID = -3176644039052730739L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 收货人
     */
    @TableField("receiver")
    private String receiver;
    @TableField("phone")
    private String phone;

    @TableField("province")
    private String province;

    @TableField("city")
    private String city;

    @TableField("area")
    private String area;
    /**
     * 详细地址
     */
    @TableField("locations")
    private String locations;

    @TableField("uid")
    private Integer uid;


}
