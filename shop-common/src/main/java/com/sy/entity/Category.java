package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
public class Category implements Serializable {
    private static final long serialVersionUID = -5809782578272943999L;

    private Integer id;
    private Integer parent;
    private String name;
    private Integer isparent;
}
