package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName(value = "packaging")
public class Packaging implements Serializable {
    private Integer id;
    private Integer pid;
    private String packaging;
}
