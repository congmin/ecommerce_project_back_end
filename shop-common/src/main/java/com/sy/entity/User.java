package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;



/**
 * @Description: 数据库对应的user实体类
 * @Author: 董星奇
 * @Date:
 */
@TableName("user")
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 4908414170877709555L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名
     */
    @TableField("userName")
    private String userName;
    @TableField("phone")
    private String phone;
    @TableField("email")
    private String email;
    @TableField("pwd")
    private String pwd;
    /**
     * 昵称
     */
    @TableField("nickName")
    private String nickName;
    /**
     * 姓名
     */
    @TableField("name")
    private String name;
    @TableField("birthday")
    private Date birthday;
    @TableField("sex")
    private Integer sex;
    /**
     * 注册时间
     */
    @TableField("joinTime")
    private String joinTime;
    /**
     * 会员等级
     */
    @TableField("level")
    private String level;
    /**
     * 积分
     */
    @TableField("integral")
    private String integral;
    /**
     * 浏览次数
     */
    @TableField("viewNum")
    private Integer viewNum;
    /**
     * 购物比
     */
    @TableField("shoppingRadio")
    private String shoppingRadio;

    /**
     * 最新消费金额
     */
    @TableField("latestConsumption")
    private BigDecimal latestConsumption;
    /**
     * 最新消费时间
     */
    @TableField("comsumptionTime")
    private Date comsumptionTime;

    /**
     * 账户安全的分值
     */
    @TableField("accountSecurity")
    private Integer accountSecurity;
    /**
     * 支付密码
     */
    @TableField("paypwd")
    private String payPwd;
    /**
     * 身份证号码
     */
    @TableField("card")
    private String card;

    /**
     * 账户状态
     */
    @TableField("status")
    private String status;

    /**
     * 地址
     */
    @TableField("address")
    private String address;


    /**
     * 余额
     */
    @TableField("balance")
    private String balance;


    /**
     * 个签
     */
    @TableField("signature")
    private String signature;

    /**
     * 最新充值时间
     */
    private  Date rechargetime;

    /**
     * 最新充值时间
     */
    private BigDecimal rechargemoney;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public String getShoppingRadio() {
        return shoppingRadio;
    }

    public void setShoppingRadio(String shoppingRadio) {
        this.shoppingRadio = shoppingRadio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public BigDecimal getLatestConsumption() {
        return latestConsumption;
    }

    public void setLatestConsumption(BigDecimal latestConsumption) {
        this.latestConsumption = latestConsumption;
    }



    public Date getComsumptionTime() {
        return comsumptionTime;
    }

    public void setComsumptionTime(Date comsumptionTime) {
        this.comsumptionTime = comsumptionTime;
    }

    public Date getRechargetime() {
        return rechargetime;
    }

    public void setRechargetime(Date rechargetime) {
        this.rechargetime = rechargetime;
    }

    public BigDecimal getRechargemoney() {
        return rechargemoney;
    }

    public void setRechargemoney(BigDecimal rechargemoney) {
        this.rechargemoney = rechargemoney;
    }

    public Integer getAccountSecurity() {
        return accountSecurity;
    }

    public void setAccountSecurity(Integer accountSecurity) {
        this.accountSecurity = accountSecurity;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", pwd='" + pwd + '\'' +
                ", nickName='" + nickName + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", sex=" + sex +
                ", joinTime='" + joinTime + '\'' +
                ", level='" + level + '\'' +
                ", integral='" + integral + '\'' +
                ", viewNum=" + viewNum +
                ", shoppingRadio='" + shoppingRadio + '\'' +
                ", latestConsumption=" + latestConsumption +
                ", comsumptionTime='" + comsumptionTime + '\'' +
                ", accountSecurity=" + accountSecurity +
                ", payPwd='" + payPwd + '\'' +
                ", card='" + card + '\'' +
                ", status='" + status + '\'' +
                ", address='" + address + '\'' +
                ", balance='" + balance + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
