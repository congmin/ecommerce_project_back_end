package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName("ordert")
public class Order implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private String orderno;
    private String productname;
    private BigDecimal totalprice;
    /**
     * 打几折  1不打折  0.几打折
     */
    private BigDecimal discount;
    /**
     * 下单时间
     */
    private Date ordertime;
    /**
     * 支付方式
     */
    private String type;
    //1是已付款，2是未付款，5未发货，6.已发货
    private String status;
    //1已退款，2待退款
    private Integer refundstatus;
    //1已发货，2未发货
    private Integer deliverstatus;
    //1已收货，2未收货
    private Integer receivingstatus;
    //1已评价，2未评价
    private Integer evaluatestatus;

    /**
     * 订单商品的总数量
     */
    private Integer quantity;
    /**
     * 订单信息
     */


    private String message;
    private Integer payid;

    private Integer addressid;

}
