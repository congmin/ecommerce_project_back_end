package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@TableName(value = "dealorder")
public class DealOrder {
    private Integer id;
    private String orderno;
    private Date ordertime;
    private BigDecimal orderprice;
}
