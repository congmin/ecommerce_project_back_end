package com.sy.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/13 20:13
 */
@TableName("level")
@Data
public class Level implements Serializable {
    private Integer id;
    private String level;
    private Integer integral;
}
