package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description: 管理员登录记录的实体类
 * @Author: 陈聪敏
 * @Date:
 */
@Data
@TableName(value = "adminloginrecord")
public class AdminLoginRecord implements Serializable {
    private static final long serialVersionUID = -6974138994249325271L;
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id ;
    private String ip;
    private String address;
    private Date logintime;
    private String content;
    private Integer aid;
}
