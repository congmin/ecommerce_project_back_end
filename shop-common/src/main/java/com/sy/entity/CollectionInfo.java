package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/12 23:40
 */
@TableName("collection")
@Data
public class CollectionInfo implements Serializable {
    private static final long serialVersionUID = 6967127051094212508L;
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    private Integer uid;
}
