package com.sy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库市表对应实体类
* @Author: 董星奇
 */
@Data
@TableName("city")
public class City implements Serializable {
    private static final long serialVersionUID = 7688650387217639595L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField("cityid")
    private Integer cityid;

    @TableField("city")
    private String city;

    @TableField("provinceid")
    private Integer provinceid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getProvinceid() {
        return provinceid;
    }

    public void setProvinceid(Integer provinceid) {
        this.provinceid = provinceid;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", cityid=" + cityid +
                ", city='" + city + '\'' +
                ", provinceid=" + provinceid +
                '}';
    }
}
