package com.sy.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/15 13:35
 */
@Data
public class MemberParams implements Serializable {
    private static final long serialVersionUID = -8276991962474313690L;
    private String userName;
    private String name;
    private Integer sex;
    private String phone;
    private String email;
    private String address;
    private String status;
}
