package com.sy.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/11 18:31
 */
@Data
public class CartProductParams implements Serializable {
    private static final long serialVersionUID = 6420251290165830936L;
    private Integer pid;
    private Integer count;
}
