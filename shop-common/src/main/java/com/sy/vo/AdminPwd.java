package com.sy.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 管理员更新密码的实体类
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class AdminPwd implements Serializable {
    private String pwd;
    private String newPwd;
    private String confirmPwd;
}
