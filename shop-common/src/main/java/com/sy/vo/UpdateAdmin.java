package com.sy.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 更改管理员的信息的实体类
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class UpdateAdmin implements Serializable {
    private String userName;
    private Integer sex;
    private Integer age;
    private String phone;
    private String qq;
    private String email;
}
