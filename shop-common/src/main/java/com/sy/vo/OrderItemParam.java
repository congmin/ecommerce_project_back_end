package com.sy.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/17 21:12
 */
@Data
public class OrderItemParam implements Serializable {
    private static final long serialVersionUID = -7490529818296493285L;
    private Integer pid;
    private String flavor;
    private String packaging;
    private Integer count;
}
