package com.sy.index;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

@Data
@Document(indexName = "brand_index", createIndex = true)
public class BrandIndex implements Serializable {
    private static final long serialVersionUID = 3123123123576L;
    @Id
    private String id;

    @Field(type = FieldType.Long, store = true)
    private Long bid;

    @Field(type = FieldType.Text, store = true)
    private String brandno;

    @Field(type = FieldType.Text, store = true)
    private String logo;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String brandname;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String country;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String describe;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private Integer status;






}
