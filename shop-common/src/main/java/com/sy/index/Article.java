package com.sy.index;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
/**
 * indexName：索引名
 * createIndex:是否要在创建Repository接口的时候同时创建索引库
 */
@Document(indexName = "article_idx", createIndex = false)
public class Article {


    /**
     * @ Id注解只是说明当前属性对应的是_id，如果实体类中要定义类似于数据库表中这种自定义的id
     * 则需给定另外的属性
     */
    @Id
    private String id;

    /**
     * 除了_id以外的自己定义的id
     * type:mapping中设置的数据类型
     * analyzer:分词器
     */
    @Field(type = FieldType.Long, store = true)
    private Long articleId;
    @Field(type = FieldType.Text, store = true, analyzer = "ik_max_word")
    private String title;
    @Field(type = FieldType.Text, store = true, analyzer = "ik_max_word")
    private String content;
}
