package com.sy.config;

import com.sy.constant.AppConstant;
import com.sy.exception.ShopSystemException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @Author 孙玮立
 * @Date
 */
@Component
public class RedisClient {

    @Value("${redis.host}")
    private String redisHost;

    @Value("${redis.port}")
    private Integer redisPort;

    private JedisPool jedisPool;

    @PostConstruct
    public void init() {
        jedisPool = new JedisPool(redisHost, redisPort);
    }


    public boolean set(String key, Serializable obj) {
        try (Jedis jedis = jedisPool.getResource()) {
            return StringUtils.equals(AppConstant.REDIS_OK_RESULT, jedis.set(key.getBytes(), SerializationUtils.serialize(obj)));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public  boolean expire(String key, int expire) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.expire(key, expire) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }


    }
    public <T> T getObj(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            byte[] bytes = jedis.get(key.getBytes());
            if (!ArrayUtils.isEmpty(bytes)) {
                return (T) SerializationUtils.deserialize(bytes);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    public  Map<String, String> hgetAll(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hgetAll(key);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }
    public  Long hset(String key, String field, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hset(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }
    public  String hget(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hget(key,field);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    /**
     * 封装了hash类型中的hincrBy命令
     *
     * @param key   hash类型在redis中的key
     * @param field hash类型中要修改的属性名
     * @param value hash类型中属性要修改的增量值
     * @return 属性更新以后的对应的value的新值
     */
    public  Long hincrBy(String key, String field, Long value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hincrBy(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    public boolean del(String... keys) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.del(keys) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    public boolean  hDel(String key,String... fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hdel(key,fields)>0;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    /**
     * string类型的get操作
     *
     * @param key 键名
     * @return 返回的键值
     */
    public String get(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }

    /**
     * string类型的incr命令
     *
     * @param key string类型的键
     * @return 自增后的结果
     */
    public  Long incr(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.incr(key);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShopSystemException("系统异常");
        }
    }


    /**
     * 生成订单编号的前缀
     *
     * @return
     */
    private String generateOrderPrefix() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        //年
        int year = c.get(Calendar.YEAR);
        //一年中的第几天
        int day = c.get(Calendar.DAY_OF_YEAR);
        //一天中的第几个小时
        int hour = c.get(Calendar.HOUR_OF_DAY);

        DecimalFormat format = new DecimalFormat("000");
        String dayFmt = format.format(day);
        format = new DecimalFormat("00");
        String hourFmt = format.format(hour);
        return year - 2000 + dayFmt + hourFmt;
    }


    /**
     * 用来生成订单编号
     *
     * @return 订单编号
     */
    public  String generateOrderId() {
        DecimalFormat format = new DecimalFormat("00000000");
        return generateOrderPrefix() + format.format(incr(AppConstant.ORDER_SEQ_REDIS_KEY));
    }
}
