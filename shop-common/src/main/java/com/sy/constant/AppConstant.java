package com.sy.constant;


public class AppConstant {
    /**
     *全部的产品信息存入redis的key
     * **/
    public static final String AllGoodInfoExceptPicture_RedisKey = "AllGoodInfoExceptPicture";

    public static final String activity_Redis_Key_Prefix="activity_pid_";

    /**
     * 登录邮箱验证
     */
    public static final String EMAIL_REGEXP = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    /**
     * 用户注册昵称默认为 :游客+8位随机字符串
     */
    public static final String PRE_NICKNAME = "游客";

    /**
     * 手机号码正则表达式
     */
    public static final String PHONE_REGEXP = "[1](([3][0-9])|([4][5-9])|([5][0-3,5-9])|([6][5,6])|([7][0-8])|([8][0-9])|([9][1,8,9]))[0-9]{8}";
    /**
     * 短信验证码字符串的长度
     */
    public static final int SHORT_MESSAGE_VALIDATE_CODE_LENGTH = 4;

    /**
     * 个人资料全部填写完毕后账户安全加50分
     */
    public static final int ACCOUNT_SECURITY=50;
    /**
     * 设置支付密码为6位数字
     */
    public static final String PAYPWD_REGEXP="\\d{6}";

    /**
     * 定义用来保存短信验证码的Session的属性名
     */
    public static final String SHORT_MESSAGE_CODE_SESSION_NAME = "short_message_code_session_name";

    /**
     * 设置支付密码时的session属性名
     */
    public static final String PAYPWD_MESSAGE_CODE="paypwd_message_code";
    /**
     * 绑定旧手机时验证码
     */
    public static final String OLD_PHONE_CODE="old_phone_message_code";
    /**
     * 绑定新手机时验证码
     */
    public static final String NEW_PHONE_CODE="new_phone_message_code";
    /**
     * 绑定邮箱时发的短信验证码
     */
    public static final String EMAIL_CODE="email_message_code";

    /**
     * 实名认证时的身份证正则
     */
    public static final String ID_CARD="^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";
    /**
     * 用户每完善信息加10分
     */
    public static final Integer USER_SCORE=10;
    /**
     * 用户的收货地址不是默认地址时的状态
     */
    public static final Integer NO_DEFAULT_ADDRESS=0;
    /**
     * 用户的收货地址是默认地址时的状态
     */
    public static final Integer DEFAULT_ADDRESS=1;
    /**
     * 用户名前缀
     */
    public static final String USER_NAME = "USER_NAME";

    /**
     * REDIS返回操作成功的结果
     */
    public static final String REDIS_OK_RESULT = "OK";

    /**
     * 用于在Redis中存储登录状态下用户对应的购物车的key的前缀
     */
    public static final String USER_CART_REDIS_PREFIX = "user_cart_";

    /**
     * 整数正则
     */
    public static final String INTEGER_REGEX = "\\d+";

    public static final String Good_Category_Redis_key = "Good_Category_Redis_key";

    /**
     * 验证码在session中的属性名
     */
    public static final String SESSION_VALIDATE_CODE = "session_validate_code";

    /**
     * 超级管理员角色
     */
    public static final String ROLE_SURPERADMIN_NAME = "ROLE_SURPERADMIN";

    /**
     * 普通管理员角色
     */
    public static final String ROLE_GENERALADMIN_NAME = "ROLE_GENERALADMIN";

    /**
     * 编辑管理员角色
     */
    public static final String ROLE_EDITADMIN_NAME = "ROLE_EDITADMIN";
    /**
     * 用户账户安全分注册时默认为0
     */
    public static final Integer USER_DEFAULT_SAFE_SCORE=0;

    /**
     * 用户收货地址为默认时为1
     */
    public static final Integer USER_DEFAULT_ADDRESS_STATUS=1;

    /**
     * 注册时用户账号默认为可用状态为1
     */
    public static final Integer USER_DEFAULT_STATUS=1;

    /**
     * 注册时用户账号会员等级默认为普通会员1
     */
    public static final Integer USER_DEFAULT_LEVEL=1;

    /**
     *注册时用户账号默认的最新消费金额
     */
    public static final Integer USER_DEFAULT_LATESTCONSUMPTION=0;

    /**
     * 注册时用户账号默认的积分为0
     */
    public static final String USER_DEFAULT_INTEGRAL="0";
    /**
     * 注册时用户账号默认的浏览次数为0
     */
    public static final Integer USER_DEFAULT_VIEWNUM=0;

    /**
     * 注册时用户账号的默认购物比为0
     */
    public static final String USER_DEFAULT_SHOPPINGRADIO="0";
    /**
     * 注册时用户账号的默认余额为0
     */
    public static final String USER_DEFAULT_BALANCE="0";

    /**
     * 注册时用户账号的默认个性签名
     */
    public static final String USER_DEFAULT_SIGNATURE="这家伙很懒，什么也没有留下";

    /**
     * Redis中用来表示订单提交次数的计数器的key的前缀
     */
    public static final String SUBMIT_ORDER_TIMER_PREFIX = "submit_order_timer_";
    /**
     * 订单提交的间隔时间
     */
    public static final Integer SUBMIT_ORDER_TIME_INTERVAL = 15;

    /**
     * Redis中用来表示订单编号中自增序列的值的key
     */
    public static final String ORDER_SEQ_REDIS_KEY = "order_seq";


    public static  final String  TEST_PUSH_STRING="测试是否提交成功";

}