package com.sy.utils;



import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * 短信平台工具类
 * @author Administrator
 */
public class SmsUtils {
    public SmsUtils() {

    }

    /**
     * 用于向用户发送手机验证码
     * @param mobile 要发送的手机号
     * @param code 验证码的值
     */
    public static void sendMail(String mobile, String code) {
        try {
            //创建连接
            URL url = new URL("http://api01.monyun.cn:7901/sms/v2/std/single_send");
            //通过HttpURLConnection调用第三方接口
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            // POST请求
            DataOutputStream out = new
                    DataOutputStream(connection.getOutputStream());
            JSONObject jsonObject = new JSONObject();
            //APIKEY
            jsonObject.put("apikey", "b33d2411219fe27c60a64e30a0dd88e7");
            //要发送到哪里去
            jsonObject.put("mobile", mobile);
            //短信的内容
            jsonObject.put("content", URLEncoder.encode("验证码：" + code + "，打死都不要告诉别人哦！", "gbk"));
            out.writeBytes(jsonObject.toJSONString());
            out.flush();
            out.close();
            // 读取响应
            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(connection.getInputStream()));
            String lines;
            StringBuffer sb = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                sb.append(lines);
            }
            JSONObject result = JSON.parseObject(sb.toString());
            System.out.println(result);
            reader.close();
            // 断开连接
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
