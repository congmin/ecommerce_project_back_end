package com.sy.utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

/**
 * 验证码工具类
 * @author Administrator
 */
public class ValidateCodeUtils {
    private ValidateCodeUtils() {

    }

    public static String getRandomStr(int length) {
        String str = "3456789abcdefghjkmnpqrstuvwxy";
        String result = "";
        Random r = new Random();
        for (int i = 1; i <= length; i++) {
            result += str.charAt(r.nextInt(str.length()));
        }
        return result;
    }

    public static Map<String, Object> drawString() {
        String code = getRandomStr(4);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("code", code);

        Random r = new Random();
        BufferedImage image = new BufferedImage(120, 30, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        g.fillRect(0, 0, 120, 30);
        g.setFont(new Font("", Font.BOLD, 30));
        g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        g.drawString(code, 20, 20);
        for (int i = 1; i < 15; i++) {
            g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
            g.drawLine(r.nextInt(120), r.nextInt(30), r.nextInt(120), r.nextInt(30));
        }
        map.put("image", image);
        return map;
    }

    public static Map<String, Object> getImg() {

        // 创建内存画板对象
        BufferedImage image = new BufferedImage(120, 40, BufferedImage.TYPE_INT_RGB);
        // 获取画笔
        Graphics g = image.getGraphics();
        // 为画笔指定颜色
        Random r = new Random();
        g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        // 为画板指定背景色
        g.fillRect(0, 0, 120, 40);
        // 取得随机字符串
        String num = getNum(4);
        // 修改画笔颜色
        g.setColor(new Color(0, 0, 0));
        // 设置字体
        g.setFont(new Font(null, Font.BOLD, 24));
        // 绘制字符串
        g.drawString(num, 35, 25);
        // 绘制干扰线
        for (int i = 1; i <= 8; i++) {
            g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
            g.drawLine(r.nextInt(100), r.nextInt(30), r.nextInt(100), r.nextInt(30));
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", num);
        map.put("img", image);
        return map;
    }

    /**
     * 方法描述: 生成随机字符串</br>
     * 初始作者: Administrator<br/>
     * 创建日期: 2016-8-4-上午11:22:30<br/>
     * 开始版本: 2.0.0<br/>
     * =================================================<br/>
     * 修改记录：<br/>
     * 修改作者 日期 修改内容<br/>
     * ================================================<br/>
     *
     * @param num
     *            字符串长度
     * @return
     *         String
     */
    private static String getNum(int num) {

        String str = "ABCDEFGHJKMNPQRSTUVWXY13456789";
        Random r = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= num; i++) {
            sb.append(str.charAt(r.nextInt(str.length())));
        }
        return sb.toString();
    }
}
