package com.sy.exception;



/**
 * @Description:
 * @Author: 陈聪敏
 * @Date:
 */
public class ShopSystemException extends RuntimeException{
    public ShopSystemException() {
    }

    public ShopSystemException(String message) {
        super(message);
    }

    public ShopSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShopSystemException(Throwable cause) {
        super(cause);
    }

    public ShopSystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
