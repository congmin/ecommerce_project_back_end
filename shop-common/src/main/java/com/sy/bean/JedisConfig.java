package com.sy.bean;

import com.sy.constant.AppConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.PostConstruct;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date: 2020 3-11
 */
@Component
public class JedisConfig {
    @Value("${redis.host}")
    public String host;
    @Value("${redis.port}")
    public String port;

    private JedisPool jedisPool;

    @PostConstruct
    public void init(){
        jedisPool=new JedisPool(host,Integer.parseInt(port));
    }
    public Boolean setString(String key,String value){
        try(Jedis jedis=jedisPool.getResource();){
            return StringUtils.equalsIgnoreCase( jedis.set(key,value), AppConstant.REDIS_OK_RESULT);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getString(String key){
        try(Jedis jedis=jedisPool.getResource();){
            return jedis.get(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Long decrBy(String key){
        try(Jedis jedis=jedisPool.getResource();){
            return jedis.decrBy(key,1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

   public void expire(String key,Integer time){
       try(Jedis jedis=jedisPool.getResource();){
           Long expire = jedis.expire(key, time);
       }catch (Exception e){
           e.printStackTrace();
       }
   }

}
