package com.sy.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description: 管理员用户的具体信息
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class AdminInfo implements Serializable {
    private Integer id;
    private String userName;
    private String phone;
    private String email;
    private Date regtime;
    private Integer status;
    private String role;
}
