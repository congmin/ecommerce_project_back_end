package com.sy.dto;

import com.sy.entity.Evaluate;
import com.sy.entity.Flavor;
import com.sy.entity.Packaging;
import com.sy.entity.Product;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 22:21
 */
@Data
public class ProductPageInfo implements Serializable {
    private static final long serialVersionUID = 6650187086978111317L;
    private Product product;
    private Integer evaluateCount;
    private Integer middleEvaluation;
    private Integer goodEvaluation;
    private Integer badEvaluation;
    private List<Flavor> flavors;
    private List<Packaging> packagings;
    private List<EvaluateInfo> evaluateInfo;
    private List<Product> lookAndSeeProducts;
    private List<Product> guessLoveProducts;
}
