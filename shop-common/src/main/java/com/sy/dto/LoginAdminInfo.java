package com.sy.dto;

import lombok.Data;

import java.sql.Date;

/**
 * @Description: 当前登录用户的具体信息，用于管理员修改自己的信息
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class LoginAdminInfo {
    private String userName;
    private Integer sex;
    private Integer age;
    private String phone;
    private String email;
    private String qq;
    private String role;
    private Date regtime;
}
