package com.sy.dto;

import com.sy.entity.Category;

import lombok.Data;


import java.io.Serializable;
import java.util.List;

@Data
public class CategoryTree implements Serializable {//大分类

private Category self;
private List<CategoryTree> children;
    public Category getSelf() {
        return self;
    }

    public void setSelf(Category self) {
        this.self = self;
    }

    public List<CategoryTree> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTree> children) {
        this.children = children;
    }
}
