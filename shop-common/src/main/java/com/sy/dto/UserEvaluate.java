package com.sy.dto;

import com.sy.entity.Product;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/20 22:15
 */
@Data
public class UserEvaluate implements Serializable {
    private static final long serialVersionUID = -6425039606319763329L;
    private String evaluate;
    private Product product;
    private String flavor;
    private String packaging;
    private String data;
}
