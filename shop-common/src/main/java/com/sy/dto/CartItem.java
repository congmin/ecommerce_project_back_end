package com.sy.dto;

import com.sy.entity.Product;
import com.sy.vo.CartProductParams;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/11 23:42
 */

@Data
public class CartItem implements Serializable {
    private static final long serialVersionUID = 5411280944087159835L;
    private Product product;
    private String flavor;
    private String packaging;
    private Integer count;
}
