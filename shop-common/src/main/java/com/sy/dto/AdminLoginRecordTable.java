package com.sy.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description: 管理员登录记录的实体类
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class AdminLoginRecordTable implements Serializable {
    private Integer id ;
    private String userName;
    private String role;
    private String ip;
    private String address;
    private Date logintime;
    private String content;
}
