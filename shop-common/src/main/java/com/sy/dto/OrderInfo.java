package com.sy.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/19 20:29
 */
@Data
public class OrderInfo implements Serializable {
    private static final long serialVersionUID = -7312158780955528505L;
    private String userName;
    private String orderNo;
    private Integer count;
    private String status;
    private Date time;
}
