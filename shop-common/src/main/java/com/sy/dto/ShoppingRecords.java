package com.sy.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/19 21:27
 */
@Data
public class ShoppingRecords implements Serializable {
    private static final long serialVersionUID = 6306047658853922546L;
    private String userName;
    private String orderNo;
    private Integer count;
    private String status;
    private String time;
}
