package com.sy.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
public class RecommendGoods {
private Integer pid;
private String productname;
private BigDecimal price;
private String cname;
private String productpicture;
}
