package com.sy.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 管理员权限，人数，描述的实体类，用于后台管理中管理员管理中的权限管理
 * @Author: 陈聪敏
 * @Date:
 */
@Data
public class AdminRights implements Serializable {
    private Integer id;
    private String role;
    private Integer peoplenum;
    private String username;
    private String descs;
}
