package com.sy.dto;

import lombok.Data;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/13 20:11
 */

@Data
public class LevelInfo {
    private Integer id;
    private String level;
    private Integer count;
}
