package com.sy.dto;

import javafx.util.converter.IntegerStringConverter;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 22:09
 */
@Data
public class EvaluateInfo implements Serializable {
    private static final long serialVersionUID = 2018920248064251621L;
    private String userName;
    private String evaluate;
    private String flavor;
    private String packaging;
    private String evaluatetime;

}
