package com.sy.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/19 20:29
 */
@Data
public class Browses implements Serializable {
    private static final long serialVersionUID = -7717384521521748017L;
    private String userName;
    private String productName;
    private BigDecimal price;
    private Integer viewNum;
    private Timestamp time;
}
