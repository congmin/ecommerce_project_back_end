package com.sy.dto;

import lombok.Data;

/**
 * @Description:
 * @Author: 陈聪敏
 * @Date: 2020/3/19 0019 21:25
 */
@Data
public class AdminSize {
    private Integer surperAdmin;
    private Integer admin;
    private Integer editAdmin;
    private Integer total;
}
