package com.sy.dto;

import com.sy.entity.Product;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
public class OrderItem {
    private Product product;
    private String flavor;
    private String packaging;
    private Integer count;
    private BigDecimal smallPrice;
}
