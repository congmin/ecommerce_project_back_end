package com.sy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Data
//FasrJson还原Java对象时需要无参构造方法，否则会出现异常
@NoArgsConstructor
//属性值如果为空在JSON中不显示
@JsonInclude(JsonInclude.Include.NON_NULL)
//因为UserDetails中有些字段在我的业务中没必要在返回结果中体现，直接忽略之
@JsonIgnoreProperties({"enabled", "accountNonLocked", "accountNonExpired", "credentialsNonExpired", "authorities"})
public class TokenUser implements UserDetails {
    private Integer uid;
    private String userName;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;
    private List<String> roles;

    public TokenUser(Integer uid, String userName, String password, List<? extends org.springframework.security.core.GrantedAuthority> authorities, List<String> roles) {
        this.uid = uid;
        this.userName = userName;
        this.password = password;
        this.authorities = authorities;
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
