package com.sy.dto;

import com.sy.entity.Product;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/20 22:51
 */
@Data
public class UserBrowse implements Serializable {
    private static final long serialVersionUID = -186459299982314277L;
    private List<Product> nowBrowses;
    private List<Product> oldBrowses;
}
