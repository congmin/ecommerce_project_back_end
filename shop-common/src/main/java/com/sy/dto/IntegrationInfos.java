package com.sy.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/19 20:29
 */
@Data
public class IntegrationInfos implements Serializable {
    private static final long serialVersionUID = 4254984150551607710L;
    private String userName;
    private String productName;
    private BigDecimal price;
    private Integer integral;
    private Date time;
}
