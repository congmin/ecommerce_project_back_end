package com.sy.controller;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.sy.dto.Result;
import com.sy.service.CartService;
import com.sy.service.GoodService;
import com.sy.vo.CartProductParams;
import com.sy.vo.OrderItemParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/11 16:23
 */

@RestController
public class CartController {

    @Autowired
    private HttpServletResponse res;

    @Qualifier("cartServiceImpl")
    @Autowired
    private CartService cartService;


    @RequestMapping(value = "carts",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result addToCart(@RequestHeader("auth-token") String token,@RequestBody OrderItemParam orderItemParam){
        System.out.println(orderItemParam);
        Result result = cartService.addToCart(token, orderItemParam);
        res.setStatus(result.getCode());
        return result ;
    }


    @RequestMapping(value = "carts",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getCartItems(@RequestHeader("auth-token") String token){
        Result result = cartService.getCartItems(token);
        res.setStatus(result.getCode());
        return  result;
    }

    @RequestMapping(value = "carts",method = RequestMethod.DELETE,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delCartItems(@RequestHeader("auth-token") String token,String pids){
        Result result = cartService.clearSomeCart(token,pids);
        System.out.println(pids);
        res.setStatus(result.getCode());
        return  result;
    }

    @RequestMapping(value = "carts",method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result uploadCartItems(@RequestHeader("auth-token") String token,String pid,String count){
        Result result =cartService.updateCartItem(token,pid,count);
        System.out.println(pid);
        res.setStatus(result.getCode());
        return  result;
    }

    @RequestMapping(value = "carts/users",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result addCollection(@RequestHeader("auth-token") String token,String pids){
        Result result = cartService.addCollections(token,pids);
        System.out.println(pids);
        res.setStatus(result.getCode());
        return  result;
    }


}
