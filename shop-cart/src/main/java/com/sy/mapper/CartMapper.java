package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Cart;
import org.springframework.stereotype.Repository;


/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/12 19:24
 */
@Repository
public interface CartMapper extends BaseMapper<Cart> {
}
