package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.CollectionInfo;
import org.springframework.stereotype.Repository;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/12 23:42
 */
@Repository
public interface CollectionMapper extends BaseMapper<CollectionInfo> {
}
