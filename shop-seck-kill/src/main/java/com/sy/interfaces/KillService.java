package com.sy.interfaces;

import com.sy.dto.Result;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

@Repository
public interface KillService {
    Result startKillTime(Integer pid, String token);
    Result getCanKillProducts();
}
