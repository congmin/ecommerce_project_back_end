package com.sy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = "com.sy.mapper")
@EnableFeignClients
public class SeckKillApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeckKillApplication.class,args);
    }
}
