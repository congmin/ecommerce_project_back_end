package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface OrderMapper extends BaseMapper<Order> {
  @Select(value = "select * from ordert where uid=#{uid} and status=1")
  Order getOrderByUid(@Param(value = "uid") Integer uid);
}
