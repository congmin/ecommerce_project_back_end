package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Product;
import com.sy.entity.SeckKill;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Repository
public interface SeckKillMapper extends BaseMapper<SeckKill> {
    @Select(value = "SELECT *from product WHERE id in (SELECT pid FROM activity WHERE isable=1) limit 0,4")
    List<Product> getSeckKillProducts();

    @Select(value = "select time from activity  where  pid=#{pid}")
    Integer getCountDownTime(@Param(value = "pid") Integer pid);

    @Select(value = "select *from activityuser where pid=#{pid} and uid=#{uid}")
    SeckKill getIsKill(@Param(value = "pid") Integer pid,@Param(value = "uid") Integer uid);

    @Insert(value = "insert into activityuser values(Null,#{pid},#{uid})")
    Integer addActivityUser(@Param(value = "pid")Integer pid,@Param(value = "uid")Integer uid);

    @Update(value = "update  activity set count=count-1 where pid=#{pid}")
    Integer updateKillCount(@Param(value = "pid") Integer pid);

}
