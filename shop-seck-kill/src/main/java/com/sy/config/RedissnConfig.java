package com.sy.config;

import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Configuration
public class RedissnConfig {
    @Bean(name = "redisson")
    public RedissonClient redissonClient(){
        Config config = new Config();
        config.useSingleServer().setAddress("192.168.174.101:6379");
        RedissonClient redisson = Redisson.create(config);
        return redisson;
    }
}
