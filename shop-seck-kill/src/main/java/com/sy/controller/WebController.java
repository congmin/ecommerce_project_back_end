package com.sy.controller;

import com.sy.mapper.SeckKillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

@Component
@ServerEndpoint("/imserver/{username}")
public class WebController {
    private Timer timer=new Timer();
    /**静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。*/
    private static int  timeout = 10;
    /**concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。*/
    private static ConcurrentHashMap<String,WebController> webSocketMap = new ConcurrentHashMap<>();
    /**与某个客户端的连接会话，需要通过它来给客户端发送数据*/
    private Session session;
    /**接收userId*/
    private String username="";
    private static  ConcurrentHashMap<String,Integer> UserKillMap=new ConcurrentHashMap<>();


//    @Autowired
//    private PaperMapper paperMapper;
    private String stauts;
    private static SeckKillMapper seckKillMapper;

    // 注入的时候，给类的 service 注入
    @Autowired
    public void setChatService(SeckKillMapper mapper) {
        WebController.seckKillMapper = mapper;
     }

     @OnOpen
    public   void startcountdown(@PathParam("username")String username,Session session) throws Exception {
        this.session=session;
        if(timeout<=0||"还剩0小时0分钟0秒".equals(stauts)){
            sendMessage("开抢了");
            onClose();
        }
        String query = session.getRequestURI().getQuery();
        String substring = query.substring(4, query.length());
         System.out.println(substring);
         if (UserKillMap.get(username)==null) {
             Integer countDownTime = seckKillMapper.getCountDownTime(Integer.parseInt(substring)) * 60;
             UserKillMap.put(username,countDownTime);
             timeout = UserKillMap.get(username);
         }
         timer.schedule(new TimerTask() {
            @Override
            public  void run() {
                long hh = timeout / 60 / 60 % 60;
            long mm = timeout / 60 % 60;
            long ss = timeout % 60;
            stauts = "还剩" + hh + "小时" + mm + "分钟" + ss + "秒";

            try {
                if (session.isOpen()){
                    System.out.println("timeout"+timeout);
                    if (timeout>=0){
                        sendMessage(stauts);

                    }else {
                        sendMessage("开枪了");
                        onClose();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
              }
            }
        }, 0, 1000);
    }

    public void sendMessage(String message) throws Exception {
        timeout--;
        this.session.getBasicRemote().sendText(message);
    }


    @OnClose
    public void onClose(){
        try {
            System.out.println("关闭了");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
