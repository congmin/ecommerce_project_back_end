package com.sy.controller;

import com.sy.dto.Result;
import com.sy.entity.Product;
import com.sy.interfaces.KillService;
import com.sy.service.SeckKillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RestController
public class SeckKIllController {
    @Autowired
    private SeckKillService seckKillService;
    @Autowired
    private KillService killService;
    @RequestMapping(value = "products/good", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProductsCanKil (){
        return seckKillService.getProductsCanKill();
    }


    @RequestMapping(value = "products/kill", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProductsCanKillFront (){
     return killService.getCanKillProducts();
    }

    @RequestMapping(value = "products/start", method = RequestMethod.POST)
    public Result  startKill(@RequestParam(value = "pid") Integer pid, @RequestHeader(value = "token")String token){
       return killService.startKillTime(pid,token);
    }

}
