import com.sy.SeckKillApplication;
import com.sy.bean.JedisConfig;
import com.sy.constant.AppConstant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeckKillApplication.class)
public class TesKill {
    @Autowired
    private JedisConfig jedisConfig;

    @Test
    public void test(){
        jedisConfig.setString(AppConstant.activity_Redis_Key_Prefix+"1","100");
        jedisConfig.setString(AppConstant.activity_Redis_Key_Prefix+"2","80");
        jedisConfig.setString(AppConstant.activity_Redis_Key_Prefix+"3","213");
        jedisConfig.setString(AppConstant.activity_Redis_Key_Prefix+"4","321");
        jedisConfig.setString(AppConstant.activity_Redis_Key_Prefix+"5","600");

    }

}
