package com.sy.contorller;

import com.sy.dto.Result;
import com.sy.entity.Product;
import com.sy.service.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RestController
public class GoodContorller {
    @Qualifier("goodServiceImpl")
    @Autowired
    private GoodService goodService;

    @RequestMapping(value = "categorytrees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public  Result getAllCategoryTrees(){
        return goodService.getAllCategoryTrees();
    }

    /**
     * 今日推荐的方法默认取4个最新的商品
     *
     * **/
    @RequestMapping(value = "products/today", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getAllTodayProducts(){
        return goodService.getAllTodayProducts();
    }



    /**
     * pid:商品id
     * 朱天宇
     * */
    @RequestMapping(value = "products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public  Product getProductsByiId(@PathVariable(value = "id") Integer id) {
        Product productsByiId = goodService.getProductsByiId(id);
        System.out.println(productsByiId);
        return productsByiId;
    }

    @RequestMapping(value = "products/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts(){
            return goodService.getProducts();
    }

    /**
     * @Description: 通过产品名称和时间获取对应商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "product", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result  getProductsByKeywordAndTime(@RequestParam(value = "keyword")String keyword,@RequestParam(value = "times")String times){
        return goodService.getProductsByKeywordAndTime(keyword,times);
    }

    /**
     * @Description: 获取分类列表
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "categorys", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result  getAllCategorys(){
        return goodService.getAllCategorys();
    }

    /**
     * @Description: 停用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStop/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result productStop(@RequestParam(value = "id")@PathVariable("id") Integer id) {
        return goodService.productStop(id);
    }

    /**
     * @Description: 启用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStart/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result productStart(@RequestParam(value = "id")@PathVariable("id") Integer id) {
        return goodService.productStart(id);
    }

    /**
     * @Description: 删除商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "delProduct", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delProduct(@RequestParam(value = "id") String id) {
        return goodService.delProduct(id);
    }

    /**
     * @Description: 获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "getBrands", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getBrands() {
        return goodService.getBrands();
    }

    /**
     * @Description: 通过品牌名，时间，是否国内品牌获取获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brand", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getBrandsByNameAndTime(String keyword,String times,String type) {
        return goodService.getBrandsByNameAndTime(keyword,times,type);
    }


    /**
     * @Description: 停用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStop/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result brandStop(@PathVariable("id")Integer id) {
        return goodService.brandStop(id);
    }

    /**
     * @Description: 启用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStart/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result brandStart(@PathVariable("id")Integer id) {
        return goodService.brandStart(id);
    }

    /**
     * @Description: 删除品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "delBrand", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delBrand(String ids) {
        return goodService.delBrand(ids);
    }


    /**
     * @Description: 获取产品的口味，包装,评价等具体信息
     * @Author: 孙玮立
     * @Date: 2020/3/16  16:29
     */
    @RequestMapping(value = "products/{id}/flavors/packages/evaluates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProductInfo(@RequestHeader("auth-token")String token, @PathVariable("id") Integer id) {
        return goodService.getProductInfo(token,id);
    }




    /**
     * 根据商品Bid查找所有商品的种类
     * */
    @RequestMapping(value = "brands/cid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public Result getAllBrandsByCid(@RequestParam("cids")String cids){
       return goodService.getAllBrandsByCid(cids);
    }


    /**
     * 根据商品cid查找所有商品的品牌
     * */
    @RequestMapping(value = "category/bid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public Result getAllCategorysByBid(@RequestParam("bids")String bids){
        return goodService.getAllCategorysByBid(bids);
    }
    /**
     * 根据商品的品牌种类查询结果分页
     * 朱天宇
     * */

    @RequestMapping(value = "products/conditon", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public  Result getAllProductsByCondtion(@RequestParam(value = "bid",required = false)Integer bid,@RequestParam(value = "cid",required = false)Integer cid,@RequestParam(value = "page",required = false)Integer page){
        System.out.println("bid"+bid);
        System.out.println("cid"+cid);
        return   goodService.getAllProductsByCondtion(bid,cid,page);
    }
    /**
     * 根据商品的销量综合排序
     * 朱天宇
     * */
    @RequestMapping(value = "products/sort", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getAllProductsByVolume(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page){
        System.out.println(page);
        return   goodService.getAllProductsByVolume(ids,page);
    }

    /**
     * 根据商品的销量和价格综合排序
     * 朱天宇
     * */
    @RequestMapping(value = "products/price", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public Result getAllProductsByPrice(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page){
        System.out.println(page);
        return goodService.getAllProductsByPrice(ids,page);
    }

    /**
     * 查询销量最高的product
     * 朱天宇
     * */
    @RequestMapping(value = "products/comment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public Result getAllProductsByComment(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page){
        System.out.println(page);
        return goodService.getAllProductsByComment(ids,page);
    }
  /**
   * 这是点击增加浏览记录的
   * **/
    @RequestMapping(value = "products/history/{pid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result historytime(@PathVariable(value = "pid") Integer pid,@RequestHeader("token") String token){
        return goodService.historytime(pid,token);
    }


    @RequestMapping(value = "products/article/today", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getTodayarticle(@RequestHeader(value = "token") String token){
       return goodService.getTodayarticle(token);
    }
    @RequestMapping(value = "products/reccommend", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getReccommend(){
        return goodService.getReccommend();
    }

}
