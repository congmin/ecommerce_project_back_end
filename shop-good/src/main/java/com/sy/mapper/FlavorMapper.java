package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Flavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 21:59
 */
@Mapper
public interface FlavorMapper extends BaseMapper<Flavor> {
}
