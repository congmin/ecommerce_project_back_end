package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.dto.RecommendGoods;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface RecommendMapper extends BaseMapper<RecommendGoods> {
    @Select("SELECT DISTINCT product.id pid,product.productName,product.originalPrice price,category.name cname,productpicture from product,category where cid in(\n" +
            "\n" +
            "SELECT  id from category where parent in(SELECT id sid from category where parent in(1)))    \n" +
            " AND product.cid=category.id LIMIT 0,6")
    List<RecommendGoods> getCakes();

    @Select("SELECT DISTINCT product.id pid,product.productName,product.originalPrice price,category.name cname,productpicture from product,category where cid in(\n" +
            "\n" +
            "SELECT  id from category where parent in(SELECT id sid from category where parent in(10)))    \n" +
            " AND product.cid=category.id LIMIT 0,6")
    List<RecommendGoods> getfruits();


    @Select("SELECT DISTINCT product.id pid,product.productName,product.originalPrice price,category.name cname,productpicture from product,category where cid in(\n" +
            "\n" +
            "SELECT  id from category where parent in(SELECT id sid from category where parent in(21)))    \n" +
            " AND product.cid=category.id LIMIT 0,6")
    List<RecommendGoods> getjianguo();

}
