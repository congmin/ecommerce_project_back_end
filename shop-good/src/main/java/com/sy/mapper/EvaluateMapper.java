package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Evaluate;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 22:06
 */
@Mapper
public interface EvaluateMapper extends BaseMapper<Evaluate> {
}
