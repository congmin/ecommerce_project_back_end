package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description: 文章的实体类
 * @Author: 陈聪敏
 * @Date: 2020/3/16 0016 21:17
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
   @Select(value = "SELECT * FROM article where status=1 ORDER BY addingTime desc  LIMIT 0,4")
  List<Article>  getAllTodayArticles();
}
