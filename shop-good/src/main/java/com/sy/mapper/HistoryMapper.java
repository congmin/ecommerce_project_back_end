package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.History;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/17 0:11
 */
public interface HistoryMapper extends BaseMapper<History> {
@Update(value = "UPDATE history set browsestime=now() WHERE pid=#{pid} and uid=#{uid}")
Integer updateHistory(@Param(value = "pid")Integer pid,@Param(value = "uid")Integer uid);

}
