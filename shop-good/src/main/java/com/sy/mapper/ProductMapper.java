package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;

/**
 * @Description: 商品的mapper类
 * @Author: 陈聪敏
 * @Date:
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

    @Select(value = "select * from product order by product.id desc limit 0,3")
    List<Product> getAllTodayProducts();

    /***
     * 根据条件来查询商品
     *（品牌种类，销量，价格，评价）
     * */

    @Select(value = "SELECT *from product where id in(\n" +
            "SELECT pid from(\n" +
            "SELECT  pid,count(pid) maxcount FROM evaluate where type=1 GROUP BY pid having maxcount=(\n" +
            "select MAX(maxcount) from (\n" +
            "SELECT  pid,count(pid) maxcount FROM evaluate where type=1 GROUP BY pid)as t))as b) limit #{page},10")
    List<Product> getAllProdcutByHotSort(@Param(value = "page")Integer page);

    @Select(value = "select * from product where id in(${s}) ORDER BY volume desc limit #{page},9")
    List<Product> getAllProductsByVolume(@Param(value = "s") String s,@Param(value = "page")Integer page);

}
