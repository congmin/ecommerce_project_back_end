package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.OrderProductRef;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 23:11
 */
public interface OrderProductRefMapper extends BaseMapper<OrderProductRef> {
}
