package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/16 23:10
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
