package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Brand;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import javax.validation.constraints.Max;
import java.util.List;

/**
 * @Description: 商品品牌的mapper类
 * @Author: 陈聪敏
 * @Date: 2020/3/15 0015 19:47
 */
@Mapper
public interface BrandMapper extends BaseMapper<Brand> {
    /**
     * 根据商品种类的id去查所有商品的牌子
     *
     */
    @Select(value = "SELECT * FROM  brand where id in (SELECT bid from product where cid in (${cid}))")
     List<Brand> getAllbrandsByCid(@Param(value = "cid")String cid);




}
