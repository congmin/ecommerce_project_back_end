package com.sy.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.bean.JedisConfig;
import com.sy.constant.AppConstant;
import com.sy.dto.*;
import com.sy.entity.*;
import com.sy.exception.NotFoundException;
import com.sy.exception.ShopSystemException;
import com.sy.index.BrandIndex;
import com.sy.index.ProductIndex;
import com.sy.mapper.*;
import com.sy.mapper.BrandMapper;
import com.sy.mapper.GoodMapper;
import com.sy.mapper.ProductMapper;
import com.sy.service.GoodService;
import com.sy.service.SearchService;
import com.sy.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Service
public class GoodServiceImpl implements GoodService {
    @Autowired
    private JedisConfig jedisConfig;
    @Autowired
    private GoodMapper goodMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private BrandMapper brandMapper;
    @Autowired
    private ArticleMapper articleMapper;
     @Reference
     private SearchService searchService;
     @Autowired
     private FlavorMapper flavorMapper;
     @Autowired
     private PackagingMapper packagingMapper;
     @Autowired
     private OrderMapper orderMapper;
     @Autowired
     private EvaluateMapper evaluateMapper;
     @Autowired
     private HistoryMapper historyMapper;
     @Autowired
     private OrderProductRefMapper orderProductRefMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private RecommendMapper recommendMapper;
    /*
     * 三级分类
     *
     * **/
    @Override
    public Result getAllCategoryTrees() {
        Result result=new Result();
        String Good_Category_Redis_key = jedisConfig.getString(AppConstant.Good_Category_Redis_key);
        if (StringUtils.isNotBlank(Good_Category_Redis_key)){
            //如果不是空的话
            List<CategoryTree> categoryTrees = JSON.parseArray(Good_Category_Redis_key, CategoryTree.class);
            result.setData(categoryTrees);
            result.setCode(200);
        }else{
            //如果为空的话
            try {
                List<Category> parentNodes = goodMapper.getCatroys(0);
                List<CategoryTree> fristlevelnodes = new ArrayList<CategoryTree>();
                for (Category category : parentNodes) {
                    CategoryTree categoryTree = new CategoryTree();
                    categoryTree.setSelf(category);
                    List<Category> secondCategorys = goodMapper.getCatroys(category.getId());
                    List<CategoryTree> secondlevel = new ArrayList<>();
                    for (Category category1 : secondCategorys) {
                        CategoryTree secondCatgory = new CategoryTree();
                        secondCatgory.setSelf(category1);
                        List<Category> thridCategorys = goodMapper.getCatroys(category1.getId());
                        List<CategoryTree> thridlevel = new ArrayList<>();
                        for (Category category2 : thridCategorys) {
                            CategoryTree thrid = new CategoryTree();
                            thrid.setSelf(category2);
                            thridlevel.add(thrid);
                        }
                        secondCatgory.setChildren(thridlevel);
                        secondlevel.add(secondCatgory);
                    }
                    categoryTree.setChildren(secondlevel);
                    fristlevelnodes.add(categoryTree);
                }
                System.out.println(fristlevelnodes);
                if (jedisConfig.setString(AppConstant.Good_Category_Redis_key, JSON.toJSONString(fristlevelnodes))) {
                    result.setCode(200);
                    result.setData(fristlevelnodes);
                }else {
                    result.setError("redis错误");
                    result.setCode(500);
                }
            }catch (Exception e){
                e.printStackTrace();
                result.setCode(500);
                result.setError("服务端错误");
            }
        }
        return result;
    }

    /**
     * 根据产品id查找某个产品的信息
     *
     * */
    @Override
    public Product getProductsByiId(Integer id) {
        Result result=new Result();
        Product productById= new Product();
        try {
            if (null==id){
                throw new Exception("参数有误");
            }
            productById = goodMapper.getProductById(id);
            result.setData(productById);
            result.setCode(200);
            System.out.println(result);
        }catch (Exception e
        ){
            e.printStackTrace();
            result.setError("400错误");
            result.setCode(500);
        }
        return productById;
    }

    @Override
    public List<Product> getProducts() {
        Result result=new Result();
        List<Product> products = goodMapper.getProducts();
        try {
            if (CollectionUtils.isEmpty(products)){
                throw new Exception("无数据");
            }else {
                return products;
            }
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return null;
    }

    @Override
    public Result getAllTodayProducts() {
        Result result=new Result();
        try{
            List<Product> allTodayProducts = productMapper.getAllTodayProducts();
            if (CollectionUtils.isEmpty(allTodayProducts)){
                throw new Exception("无数据");
            }
            result.setCode(200);
            result.setData(allTodayProducts);
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }

    @Override
    public Result getProductsByKeywordAndTime(String keyword, String times) {
        Result result=new Result();
        try {
            QueryWrapper<Product> wrapper = new QueryWrapper<>();
            if(!StringUtils.isBlank(keyword)){
                wrapper.like("productname",keyword);
            }
            if(!StringUtils.isBlank(times)){
                wrapper.eq("joiningtime",times);
            }
            List<Product> products = productMapper.selectList(wrapper);
            result.setCode(200);
            result.setData(products);
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }


    /**
     * @Description: 获取所有分类
     * @Author: 陈聪敏
     * @Date:
     */
    @Override
    public Result getAllCategorys() {
        Result result=new Result();
        result.setCode(200);
        result.setData(goodMapper.selectList(null));
        return result;
    }

    /**
     * @Description: 停用产品
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:33
     * 停用ES里面的product 将状态设置为0
     */
    @Override
    public Result productStop(Integer id) {
        Result result = new Result();
        try {
            Product product = new Product();
            product.setId(id);
            product.setStatus(0);
            productMapper.updateById(product);
            ProductIndex byPid = searchService.findByPid(id);
            byPid.setStatus(0);
            searchService.save(byPid);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 启用产品
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:32
     */
    @Override
    public Result productStart(Integer id) {
        Result result = new Result();
        try {
            Product product = new Product();
            product.setId(id);
            product.setStatus(1);
            productMapper.updateById(product);
            ProductIndex byPid = searchService.findByPid(id);
            byPid.setStatus(1);
            searchService.save(byPid);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 删除商品
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:32
     */
    @Override
    public Result delProduct(String ids) {
        Result result = new Result();
        try {
            String[] idsInfo = ids.split(",");
            List<Integer> idList = new ArrayList<>();
            for (int i = 0; i < idsInfo.length; i++) {
                idList.add(Integer.parseInt(idsInfo[i]));
            }
            System.out.println(idList);
            for(Integer id:idList){
                searchService.deleteByPid(id);
            }
          productMapper.deleteBatchIds(idList);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @Override
    public Result getBrands() {
        Result result = new Result();
        try {
            List<Brand> brands = brandMapper.selectList(null);
            result.setCode(200);
            result.setData(brands);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 通过品牌名，时间，是否国内品牌获取获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @Override
    public Result getBrandsByNameAndTime(String keyword, String times, String type) {
        Result result = new Result();
        try {
            QueryWrapper<Brand> wrapper = new QueryWrapper<>();
            if(!StringUtils.isBlank(keyword)){
                wrapper.like("brandname",keyword);
            }
            if(!StringUtils.isBlank(times)){
                wrapper.eq("jointime",times);
            }
            if(!StringUtils.isBlank(type)){
                if(StringUtils.equals("1",type)){
                    wrapper.eq("country","中国");
                }else{
                    wrapper.notIn("country","中国");
                }
            }
            List<Brand> brands = brandMapper.selectList(wrapper);
            result.setCode(200);
            result.setData(brands);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }


    /**
     * @Description: 停用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @Override
    @Transactional
    public Result brandStop(Integer id) {
        Result result = new Result();
        try {
            Brand brand = new Brand();
            brand.setId(id);
            brand.setStatus(0);
            brandMapper.updateById(brand);
            UpdateWrapper<Product> wrapper = new UpdateWrapper<>();
            wrapper.eq("bid", id);
            Product product = new Product();
            product.setBid(id);
            product.setStatus(0);
            BrandIndex byBid = searchService.findByBid(id);
            byBid.setStatus(0);
            searchService.save(byBid);
            List<ProductIndex> allByBid = searchService.findAllByBid(id);
            for (ProductIndex productIndex:allByBid){
                productIndex.setStatus(0);
                 searchService.save(productIndex);
            }

            productMapper.update(product, wrapper);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 启用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @Override
    public Result brandStart(Integer id) {
        Result result = new Result();
        try {
            Brand brand = new Brand();
            brand.setId(id);
            brand.setStatus(1);
            brandMapper.updateById(brand);
            UpdateWrapper<Product> wrapper = new UpdateWrapper<>();
            wrapper.eq("bid", id);
            Product product = new Product();
            product.setBid(id);
            product.setStatus(1);
            productMapper.update(product, wrapper);
            BrandIndex byBid = searchService.findByBid(id);
            byBid.setStatus(1);
            searchService.save(byBid);
            List<ProductIndex> allByBid = searchService.findAllByBid(id);
            for (ProductIndex productIndex:allByBid){
                productIndex.setStatus(1);
                searchService.save(productIndex);
            }
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    /**
     * @Description: 删除品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     *
     * 修改朱天宇
     * 删除ES里面信息
     */
    @Override
    @Transactional
    public Result delBrand(String ids) {
        Result result = new Result();
        try {
            System.out.println(ids);
            String[] idsInfo = ids.split(",");
            List<Integer> idList = new ArrayList<>();
            for (int i = 0; i < idsInfo.length; i++) {
                idList.add(Integer.parseInt(idsInfo[i]));
                UpdateWrapper<Product> wrapper = new UpdateWrapper<>();
                wrapper.eq("bid", idsInfo[i]);
                productMapper.delete(wrapper);
            }
            if (idList.size()==1){
                searchService.deleteProductByBid(idList.get(0));
            }else {
                for (Integer id:idList){
                    searchService.deleteProductByBid(id);
                }
            }
            brandMapper.deleteBatchIds(idList);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }


    /**
     * 根据商品Bid查找所有商品的种类
     * */

    @Override
    public Result getAllBrandsByCid(String cids) {
        Result result=new Result();
        try{
            List<Brand> allbrandsByCid = brandMapper.getAllbrandsByCid(cids);
            if (CollectionUtils.isEmpty(allbrandsByCid)){
                throw new Exception("未查到数据");
            }
            result.setData(allbrandsByCid);
            result.setCode(200);
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }


    /**
     * 根据商品cid查找所有商品的品牌
     * */
    @Override
    public Result getAllCategorysByBid(String bids) {
        Result result=new Result();
        try{
            List<Category> allbrandsByCid = goodMapper.getAllCategorysByBid(bids);
            if (CollectionUtils.isEmpty(allbrandsByCid)){
                throw new Exception("未查到数据");
            }
            result.setData(allbrandsByCid);
            result.setCode(200);
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;

    }

    @Override
    public Result getAllProductsByCondtion(Integer bid, Integer cid, Integer page) {
        Result result=new Result();
        if (null==page){
            page=1;
        }
        Page<Product> productPage=new Page<>((page-1)*10,10);
        QueryWrapper<Product> queryWrapper=new QueryWrapper();
        List<Object> list=new ArrayList<>();
        try {
            if (null == bid && null == cid) {
                throw new Exception("无参数");
            }
            if (null != cid && null != bid) {
                QueryWrapper<Product> byid = queryWrapper.select("*").eq("bid", bid).eq("cid", cid);
                Page<Product> productPage1 = productMapper.selectPage(productPage, byid);
                List<Product> records = productPage1.getRecords();
                if (CollectionUtils.isEmpty(records)){
                    throw  new Exception("未查到数据");
                }
                result.setCode(200);
                long size = productPage1.getSize();
                if (size%10==0){
                    list.add(size/10);
                }else {
                    list.add(size/10+1);

                }
                list.add(records);
                result.setData(list);
            } else if (null == bid) {
                QueryWrapper<Product> byid = queryWrapper.select("*").eq("cid", cid);
                Page<Product> productPage1 = productMapper.selectPage(productPage, byid);
                List<Product> records = productPage1.getRecords();
                System.out.println("bid为空的情况下"+records);
                if (CollectionUtils.isEmpty(records)){
                    throw  new Exception("未查到数据");
                }
                result.setCode(200);
                long size = productPage1.getSize();
                if (size%10==0){
                    list.add(size/10);
                }else {
                    list.add(size/10+1);

                }
                list.add(records);
                result.setData(list);
            } else if (null == cid) {
                QueryWrapper<Product> byid = queryWrapper.select("*").eq("bid", bid);
                Page<Product> productPage1 = productMapper.selectPage(productPage, byid);
                List<Product> records = productPage1.getRecords();
                System.out.println("cid为空的情况下"+records);
                if (CollectionUtils.isEmpty(records)){
                    throw  new Exception("未查到数据");
                }
                result.setCode(200);
                long size = productPage1.getSize();
                if (size%10==0){
                    list.add(size/10);
                }else {
                    list.add(size/10+1);

                }
                list.add(records);
                result.setData(list);

            }
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }



    @Override
    public Result getAllProductsByPrice(String ids, Integer page) {
        Result result = new Result();
        if (null==page){
            page=1;
        }
        Page<Product> productPage = new Page<>((page - 1) * 10, 10);
        QueryWrapper<Product> queryWrapper = new QueryWrapper();
        String[] split = ids.split(",");
        QueryWrapper<Product> productQueryWrapper = queryWrapper.select("*").in("id", split).orderByAsc("originalprice");
        List<Object> list=new ArrayList<>();
        try {
            if (null == ids) {
                throw new Exception("无参数");
            }
            if (null != ids) {
                Page<Product> productPage1 = productMapper.selectPage(productPage, productQueryWrapper);
                List<Product> records = productPage1.getRecords();
                if (CollectionUtils.isEmpty(records)){
                    throw new Exception("未查到数据");
                }
                long size = productPage1.getSize();
                if (size%10==0){
                    list.add(size/10);
                }else {
                    list.add(size/10+1);

                }
                list.add(records);
                result.setData(list);
                result.setCode(200);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }

    @Override
    public Result getAllProductsByVolume(String s, Integer page) {
        Result result = new Result();
        if (null==page){
            page=1;
        }
        Page<Product> productPage = new Page<Product>((page-1)*10 ,10);
        QueryWrapper<Product> queryWrapper = new QueryWrapper();
        String[] split = s.split(",");
        QueryWrapper<Product> productQueryWrapper = queryWrapper.select("*").in("id", split).orderByDesc("volume");
        List<Object> list =new ArrayList<>();

        try {
            if (null == s) {
                throw new Exception("无参数");
            }
            if (null != s) {
                Page<Product> productPage1 = productMapper.selectPage(productPage, productQueryWrapper);
                List<Product> records = productPage1.getRecords();
                if (CollectionUtils.isEmpty(records)){
                    throw new Exception("未查到数据");
                }
                long size = productPage1.getSize();

                if (size%10==0){
                    list.add(size/10);
                }else{
                    list.add(size/10+1);
                }
                list.add(records);
                result.setData(list);
                result.setCode(200);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }




    @Override
    public Result getAllProductsByComment(String s,Integer page) {
        Result result = new Result();
        if (null==page){
            page=1;
        }
        Page<Product> productPage = new Page<Product>((page-1)*10 ,10);
        QueryWrapper<Product> queryWrapper = new QueryWrapper();
        String[] split = s.split(",");
        QueryWrapper<Product> productQueryWrapper = queryWrapper.select("*").in("id", split).orderByDesc("goodadvice");
        List<Object> list =new ArrayList<>();

        try {
            if (null == s) {
                throw new Exception("无参数");
            }
            if (null != s) {
                Page<Product> productPage1 = productMapper.selectPage(productPage, productQueryWrapper);
                List<Product> records = productPage1.getRecords();
                if (CollectionUtils.isEmpty(records)){
                    throw new Exception("未查到数据");
                }
                long size = productPage1.getSize();

                if (size%10==0){
                    list.add(size/10);
                }else{
                    list.add(size/10+1);
                }
                list.add(records);
                result.setData(list);
                result.setCode(200);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }

    /**
     * 获取产品的包装，口味等具体信息
     * @Author: 孙玮立
     * @param id
     * @param token
     * @return
     */
    @Override
    public Result getProductInfo(String token,Integer id) {
        Result result = new Result();
        try {
            ProductPageInfo productPageInfo = new ProductPageInfo();
            Product product = productMapper.selectById(id);
            QueryWrapper<Flavor> flavorQueryWrapper = new QueryWrapper<>();
            flavorQueryWrapper.eq("pid",id);
            List<Flavor> flavors = flavorMapper.selectList(flavorQueryWrapper);
            QueryWrapper<Packaging> packagingQueryWrapper = new QueryWrapper<>();
            packagingQueryWrapper.eq("pid",id);
            List<Packaging> packagings = packagingMapper.selectList(packagingQueryWrapper);
            QueryWrapper<Evaluate> evaluateQueryWrapper =  new QueryWrapper<>();
            evaluateQueryWrapper.eq("pid", id);
            List<Evaluate> evaluates = evaluateMapper.selectList(evaluateQueryWrapper);
            Integer count = evaluateMapper.selectCount(evaluateQueryWrapper);
            List<EvaluateInfo> evaluateInfos = new ArrayList<>();
            for(Evaluate evaluate:evaluates){
                Integer uid = evaluate.getUid();
                Integer pid = evaluate.getPid();
                String type = evaluate.getType();
                QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("uid",uid);
                List<Order> orders = orderMapper.selectList(orderQueryWrapper);
                for (Order order:orders){
                    EvaluateInfo evaluateInfo = new EvaluateInfo();
                    evaluateInfo.setEvaluate(evaluate.getEvaluate());
                    evaluateInfo.setEvaluatetime(evaluate.getEvaluatetime());
                    Integer oid = order.getId();
                    QueryWrapper<OrderProductRef> orderProductRefQueryWrapper = new QueryWrapper<>();
                    orderProductRefQueryWrapper.eq("oid",oid).eq("pid",pid);
                    OrderProductRef orderProductRef = orderProductRefMapper.selectOne(orderProductRefQueryWrapper);
                    Integer fid = orderProductRef.getFid();
                    Integer packid = orderProductRef.getPackid();
                    Flavor flavor = flavorMapper.selectById(fid);
                    Packaging packaging = packagingMapper.selectById(packid);
                    evaluateInfo.setFlavor(flavor.getFlavor());
                    evaluateInfo.setPackaging(packaging.getPackaging());
                    User user =  userService.getUser(String.valueOf(uid));
                    evaluateInfo.setUserName(user.getUserName());
                    evaluateInfos.add(evaluateInfo);
                }
            }

            QueryWrapper<Evaluate> evaluateQueryWrapper1 = new QueryWrapper<>();
            evaluateQueryWrapper1.eq("pid",id).eq("type",1);
            Integer goodNum = evaluateMapper.selectCount(evaluateQueryWrapper1);
            QueryWrapper<Evaluate> evaluateQueryWrapper2 = new QueryWrapper<>();
            evaluateQueryWrapper1.eq("pid",id).eq("type",2);
            Integer middleNum = evaluateMapper.selectCount(evaluateQueryWrapper2);
            QueryWrapper<Evaluate> evaluateQueryWrapper3 = new QueryWrapper<>();
            evaluateQueryWrapper1.eq("pid",id).eq("type",3);
            Integer badNum = evaluateMapper.selectCount(evaluateQueryWrapper3);


            QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByDesc("clickCount");
            List<Product> products = productMapper.selectList(queryWrapper);
            List<Product> guessLoveProducts = new ArrayList<>();
            for (int i = 0;i<5;i++){
                guessLoveProducts.add(products.get(i));
            }

            User user = userService.LoginUser(token);
            QueryWrapper<History> historyQueryWrapper = new QueryWrapper<>();
            historyQueryWrapper.eq("uid",user.getId());
            List<History> histories = historyMapper.selectList(historyQueryWrapper);
            List<Product> lookAndSeeProducts = new ArrayList<>();
            for (History history:histories){
                Product historyProduct = productMapper.selectById(history.getPid());
                lookAndSeeProducts.add(historyProduct);
            }
            productPageInfo.setProduct(product);
            productPageInfo.setEvaluateInfo(evaluateInfos);
            productPageInfo.setBadEvaluation(badNum);
            productPageInfo.setGoodEvaluation(goodNum);
            productPageInfo.setMiddleEvaluation(middleNum);
            productPageInfo.setFlavors(flavors);
            productPageInfo.setPackagings(packagings);
            productPageInfo.setEvaluateCount(count);
            productPageInfo.setGuessLoveProducts(guessLoveProducts);
            productPageInfo.setLookAndSeeProducts(lookAndSeeProducts);

            result.setData(productPageInfo);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        return result;
    }

    @Override
    public Result historytime(Integer pid, String token) {
        Result result=new Result();
        try {
            User user = userService.LoginUser(token);
            QueryWrapper<History> queryWrapper=new QueryWrapper();
            Integer id = user.getId();
            queryWrapper.select("*").eq("pid",pid).eq("uid", id);
            History history = historyMapper.selectOne(queryWrapper);
            if (null!=history){
                historyMapper.updateHistory(pid,id);
            }else {
                History history1=new History();
                history1.setPid(pid);
                history1.setUid(id);
                history1.setBrowsestime(new Timestamp(System.currentTimeMillis()));
                historyMapper.insert(history1);
            }
           result.setCode(200);
        }catch (Exception e){
            e.printStackTrace();
        }
       return  result;

    }

    @Override
    public Result getTodayarticle(String token) {
         Result result=new Result();
         List<Object> list=new ArrayList<>();
         try{
             List<Article> allTodayArticles = articleMapper.getAllTodayArticles();
             if (CollectionUtils.isEmpty(allTodayArticles)){
                throw new NotFoundException("未找到相关文章");
             }
             User user = userService.LoginUser(token);
             list.add(user.getName());
             list.add(allTodayArticles);
             result.setData(list);
             result.setCode(200);
         }catch (NotFoundException e) {
             e.printStackTrace();
             result.setCode(500);
             result.setError(e.getMessage());
         }
         return result;
    }



    /**
     * 以下方法是获得首页推荐的物品
     * */

    @Override
    public Result getReccommend() {
        Result result=new Result();
        try{
            List<Object> list=new ArrayList<>();
            List<RecommendGoods> cakes = recommendMapper.getCakes();
            List<RecommendGoods> getfruits = recommendMapper.getfruits();
            List<RecommendGoods> getjianguo = recommendMapper.getjianguo();
            list.add(cakes);list.add(getfruits);list.add(getjianguo);
            result.setData(list);
            result.setCode(200);
        }catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }

    return result;
    }


}
