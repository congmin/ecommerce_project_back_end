import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.GoodApplication;
import com.sy.entity.Product;
import com.sy.mapper.GoodMapper;
import com.sy.mapper.ProductMapper;
import com.sy.service.GoodService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.security.krb5.internal.PAData;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GoodApplication.class)
public class TestSearch {
    @Autowired
    private ProductMapper productMapper;
    @Test
    public void method1(){
//        System.out.println(productMapper.getAllProdcutByContional());
        Page<Product> productPage=new Page<>(10,10);
        QueryWrapper<Product> queryWrapper=new QueryWrapper();
        QueryWrapper<Product> bid = queryWrapper.select("*").eq("bid", 1);
        Page<Product> productPage1 = productMapper.selectPage(productPage, bid);
        System.out.println(productPage1.getRecords());
    }

}
