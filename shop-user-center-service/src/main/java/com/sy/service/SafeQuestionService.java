package com.sy.service;

import com.sy.dto.Result;


/**
 * 安全问题接口
 * @author 董星奇
 */
public interface SafeQuestionService {
    /**
     * 获得所有安全问题
     * @author 董星奇
     */
    Result allQuestions();
}
