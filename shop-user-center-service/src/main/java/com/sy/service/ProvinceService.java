package com.sy.service;

import com.sy.dto.Result;

/**
 * 省份服务层接口
 * @author 董星奇
 */
public interface ProvinceService {
    /**
     * 获得所有省份
     * @author 董星奇
     */
    Result getAllProvinces();
}
