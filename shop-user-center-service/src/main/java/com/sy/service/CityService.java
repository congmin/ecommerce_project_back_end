package com.sy.service;


import com.sy.dto.Result;

/**
 * 市服务层接口
 * @author 董星奇
 */
public interface CityService {
    /**
     * 获得所有市
     * @author 董星奇
     */
    Result getAllCities();
}
