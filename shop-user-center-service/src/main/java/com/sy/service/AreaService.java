package com.sy.service;

import com.sy.dto.Result;

/**
 * 区服务层接口
 * @author 董星奇
 */
public interface AreaService {
    /**
     * 获得所有区
     * @author 董星奇
     */
    Result getAllAreas();
}
