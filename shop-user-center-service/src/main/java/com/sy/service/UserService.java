package com.sy.service;

import com.sy.dto.Result;
import com.sy.entity.User;
import com.sy.vo.MemberParams;
import com.sy.entity.*;
import com.sy.vo.ReceiverParam;
import com.sy.vo.UpdateReveiverParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 用户中心服务类的接口
 * @Author: 董星奇
 */
@FeignClient(value = "user-center-feign")
public interface UserService {
    /**
     * 邮箱注册
     * @param email 邮箱
     * @param userName 姓名
     * @param emailpassword  密码
     * @param passwordRepeat 确认密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "registByEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result registByEmail(@RequestParam("email") String email, @RequestParam("userName") String userName, @RequestParam("emailpassword") String emailpassword, @RequestParam("passwordRepeat") String passwordRepeat);

    /**
     *注册时手机验证码
     * @param phone 手机号码
     * @Author: 董星奇
     */
    @RequestMapping(value = "sendMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result sendMessage(String phone);

    /**
     * 手机号码注册
     * @param phone  手机号码
     * @param code 输入的验证码
     * @param phonepassword 密码
     * @param phonepasswordRepeat 确认密码
     * @param codetoken 手机收到的验证码
     * @param userName 用户名
     * @Author: 董星奇
     */
    @RequestMapping(value = "registByPhone",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result registByPhone(@RequestParam("phone") String phone, @RequestParam("code") String code, @RequestParam("phonepassword") String phonepassword, @RequestParam("phonepasswordRepeat") String phonepasswordRepeat, @RequestParam("codetoken") String codetoken, @RequestParam("userName") String userName);

    /**
     * 登录
     * @param message 登录账号：手机号码、邮箱、姓名
     * @param password 密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "login", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    Result login(@RequestParam("message") String message, @RequestParam("password") String password);

    /**
     * 获取当前登录的用户
     * @param token
     * @Author: 董星奇
     */
    @RequestMapping(value = "loginUser",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    User LoginUser(@RequestParam("token") String token);

    /**
     * 忘记密码
     * @param forgetMessage 手机号码或是邮箱
     * @Author: 董星奇
     */
    @RequestMapping(value = "forget",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    Result forgetPassword(@RequestParam("forgetMessage") String forgetMessage);
    /**
     * 个人中心资料修改
     * @param nickname 昵称
     * @param name 名字
     * @param sex 性别
     * @param birthday 生日
     * @param phone 手机号码
     * @param email 邮箱
     @Author: 董星奇
     */
    @RequestMapping(value = "user/{id}",method = RequestMethod.POST)
    Result updateUserInfo(@PathVariable("id") Integer id, @RequestParam("nickname") String nickname, @RequestParam("name") String name, @RequestParam("sex") Integer sex, @RequestParam("birthday") String birthday, @RequestParam("phone") String phone, @RequestParam("email") String email);
    //Result updateUserInfo(Integer id,String nickname,String name,Integer sex,String birthday,String phone,String email);

    /**
     * 修改用户密码
     * @param id 当前用户id
     * @param oldPwd 原密码
     * @param newPwd 新密码
     * @param confirmPwd  确认密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/user",method = RequestMethod.POST)
    public Result updateUserPwd(@RequestParam("id") Integer id, @RequestParam("oldPwd") String oldPwd, @RequestParam("newPwd") String newPwd, @RequestParam("confirmPwd") String confirmPwd);

    /**
     * 设置支付密码
     *
     * @param id            当前用户id
     * @param code          输入的验证码
     * @param payPwd        支付密码
     * @param confirmPayPwd 确认支付密码
     * @param phonecode 手机收到的验证码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/user/user", method = RequestMethod.POST)
    public Result setUPayPwd(@RequestParam("id") Integer id, @RequestParam("code") String code, @RequestParam("payPwd") String payPwd, @RequestParam("confirmPayPwd") String confirmPayPwd, @RequestParam("phonecode") String phonecode);

    /**
     * 更改手机号码
     * @param id 当前用户id
     * @param phone 旧的手机号码
     * @param oldcode 旧手机输入的验证码
     * @param bindPhone 新绑定的手机
     * @param code  新手机输入的验证码
     * @Author: 董星奇
     */
    @RequestMapping(value = "users",method = RequestMethod.POST)
    public Result updatePhone(@RequestParam("id") Integer id, @RequestParam("phone") String phone, @RequestParam("oldcode") String oldcode, @RequestParam("bindPhone") String bindPhone, @RequestParam("code") String code, @RequestParam("oldSessionCode") String oldSessionCode, @RequestParam("newSessionCode") String newSessionCode);
    /**
     * 绑定邮箱操作
     * @param id 当前用户的id
     * @param email 邮箱
     * @param code 输入的验证码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/users",method = RequestMethod.POST)
    public Result bindEmail(@RequestParam("id") Integer id, @RequestParam("email") String email, @RequestParam("code") String code, @RequestParam("sessionCode") String sessionCode);
    /**
     * 实名认证
     * @param id 当前用户id
     *  @param phone 手机号码
     * @param name 真实姓名
     * @param card 身份证号码
     * @Author: 董星奇
     */
    @RequestMapping(value = "users/users",method = RequestMethod.POST)
    public Result realName(@RequestParam("id") Integer id, @RequestParam("phone") String phone, @RequestParam("name") String name, @RequestParam("card") String card);

    /**
     * 用户填写安全问题
     * @param id 当前用户id
     * @param questionId1 问题1id
     * @param answer1 问题1答案
     * @param questionId2 问题2id
     * @param answer2 问题2答案
     * @Author: 董星奇
     */
    @RequestMapping(value = "safequestion",method = RequestMethod.POST)
    public Result addSafeQuestions(@RequestParam("id") Integer id, @RequestParam("questionId1") Integer questionId1, @RequestParam("answer1") String answer1, @RequestParam("questionId2") Integer questionId2, @RequestParam("answer2") String answer2);

    /**
     * 获取所有省份
     * @Author: 董星奇
     */
    @RequestMapping(value = "provinces",method = RequestMethod.GET)
    public List<Province> getAllProvinces();

    /**
     * 获取选中省份的所有城市
     * @Author: 董星奇
     */
    @RequestMapping(value = "cities",method = RequestMethod.POST)
    public List<City> getCitiesByProvinceid(@RequestParam("provinceid") Integer provinceid);

    /**
     * 获取所有区县
     * @Author: 董星奇
     */
    @RequestMapping(value = "areas",method = RequestMethod.POST)
    public List<Area> getAreasByCityid(@RequestParam("cityid") Integer cityid);

    /**
     * 用户新增收货地址
     * @param receiverParam 收货地址实体类
     * @Author: 董星奇
     */
    @RequestMapping(value = "location",method = RequestMethod.POST)
    public Result addReceiverAddress(@RequestBody ReceiverParam receiverParam);
    /**
     * 获取当前用户的收货地址
     * @param token token
     * @Author: 董星奇
     */
    @RequestMapping(value = "locations",method = RequestMethod.POST)
    public List<Address> getAllAddressesByUserId(@RequestParam("token") String token);

    /**
     * 获取全部用户信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.GET)
    Result getUsers();


    /**
     * 获取符合查询条件的用户信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users/keyWords",method = RequestMethod.GET)
    Result getUsersByKeyWord(@RequestParam("keyWord")String keyWord);

    /**
     * 获取符合查询条件的用户信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users/keyWords/times",method = RequestMethod.GET)
    Result getUsersByKeyWordAndTime(@RequestParam("keyWord")String keyWord,@RequestParam("time")String time);


    /**
     * 获取全部会员信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users/levels",method = RequestMethod.GET)
    Result getLevelCount();


     /**
     * 删除用户信息
     * @Author 孙玮立
     * @param ids 用户ids
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.DELETE)
    Result deleteUsers(@RequestParam("ids") String ids);


    /**
     * @Author 孙玮立
     * 更改用户账户状态
     * @param id 用户id
     * @param status 启用状态
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.PUT)
    Result uploadUser(@RequestParam("id") String id, @RequestParam("status") String status);


    /**
     * 删除用户指定要删除的收货地址
     * @param token token
     * @param addressid 地址id
     * @Author 董星奇
     */
    @RequestMapping(value = "user/location",method = RequestMethod.POST)
    public Integer delAddressById(@RequestParam("token") String token, @RequestParam("addressid") Integer addressid);

    /**
     * @Author 孙玮立
     * 添加会员用户
     * @param memberParams 会员的具体信息
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.POST)
    Result addUser(@RequestBody MemberParams memberParams);


    /**
     * @Author 孙玮立
     * 获得会员用户信息
     * @param id 会员编号
     * @return
     */
    @RequestMapping(value = "users/{id}",method = RequestMethod.GET)
    User getUser(@RequestParam("id")@PathVariable("id")String id);


    /**
     * @Author 孙玮立
     * 修改会员用户信息
     * @param id 会员编号
     * @return
     */
    @RequestMapping(value = "users/{id}",method = RequestMethod.PUT)
    Result uploadUserById(@RequestParam("id") @PathVariable("id") String id, @RequestBody MemberParams memberParams);

    /**
     * @Author 孙玮立
     * 清除会员用户的购物记录
     * @param ids 会员编号
     * @return
     */
    @RequestMapping(value = "users/shopRecords",method = RequestMethod.POST)
    Result clearRecords(@RequestParam("ids") String ids);

    /**
     * 设置用户指定的默认收货地址
     * @param token token
     * @param addressid 地址id
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/locations",method = RequestMethod.POST)
    public Integer setDefaultAddressById(@RequestParam("token") String token, @RequestParam("addressid") Integer addressid);

    /**
     * 用户编辑指定的地址
     * @param updateReveiverParam  编辑地址实体类
     * @Author: 董星奇
     */
    @RequestMapping(value = "location/location",method = RequestMethod.POST)
    public Integer updateAddress(@RequestBody UpdateReveiverParam updateReveiverParam);

    /**
     * @Description: 获取用户的账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.GET)
    Result getUserRecords();

    /**
     * @Description: 通过用户名过获取账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.POST)
    Result getUserRecordsByCondition(@RequestParam("keyword") String keyword);

    /**
     * @Description: 管理员给用户退款
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/refund",method = RequestMethod.PUT)
    Result refund(@RequestParam("uid") Integer uid,@RequestParam("money")BigDecimal money);

    /**
     * @Description: 获取用户的收藏
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/collections",method = RequestMethod.GET)
    Result getUserCollections(@RequestParam("token") String token);

    /**
     * @Description: 获取用户的浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/browses",method = RequestMethod.GET)
    Result getUserBrowse(@RequestParam("token") String token);

    /**
     * @Description: 根据用户id和商品id删除浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/browses/{id}",method = RequestMethod.DELETE)
    Result delUserBrowse(@RequestParam("token") String token,@RequestParam("id")Integer id);


    /**
     * @Description: 获取用户的评价记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/evaluates",method = RequestMethod.GET)
    Result getUserEvaluate(@RequestParam("token") String token);
}
