package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:  权限的mapper类
 * @Author: 陈聪敏
 * @Date:
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
}
