package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Article;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 文章的实体类
 * @Author: 陈聪敏
 * @Date: 2020/3/16 0016 21:17
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
}
