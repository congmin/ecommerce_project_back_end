package com.sy.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.AdminRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 管理员和权限的mapper类
 * @Author: 陈聪敏
 * @Date:
 */
@Mapper
public interface AdminRoleMapper extends BaseMapper<AdminRole> {
}
