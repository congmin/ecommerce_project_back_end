package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.AdminLoginRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminLoginRecordMapper extends BaseMapper<AdminLoginRecord> {
}
