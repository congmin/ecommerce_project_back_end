package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Sort;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 分类信息的mapper类
 * @Author: 陈聪敏
 * @Date: 2020/3/16 0016 21:56
 */
@Mapper
public interface SortMapper extends BaseMapper<Sort> {
}
