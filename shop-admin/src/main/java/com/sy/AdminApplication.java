package com.sy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description: 管理员的启动类
 * @Author: 陈聪敏
 * @Date:
 */
@SpringBootApplication
@MapperScan("com.sy.mapper")
@EnableTransactionManagement
@ServletComponentScan
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.sy.service")
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class,args);
    }
}
