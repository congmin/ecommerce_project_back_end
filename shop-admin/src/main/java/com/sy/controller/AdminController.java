package com.sy.controller;

import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;
import com.sy.bean.JedisConfig;
import com.sy.constant.AppConstant;
import com.sy.dto.Result;
import com.sy.entity.Product;
import com.sy.entity.User;
import com.sy.exception.ShopSystemException;
import com.sy.service.AdminService;
import com.sy.service.GoodService;
import com.sy.service.UserService;
import com.sy.service.rpc.GoodServiceRpc;
import com.sy.service.rpc.OrderServiceRpc;
import com.sy.utils.HttpUtils;
import com.sy.utils.ValidateCodeUtils;
import com.sy.vo.AdminPwd;
import com.sy.vo.MemberParams;
import com.sy.vo.UpdateAdmin;
import org.aspectj.weaver.patterns.IToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Description: 管理员的控制层
 * @Author: 陈聪敏
 * @Date:
 */
@RestController
@RequestMapping("v1.0")
public class AdminController {
    @Autowired
    private HttpServletResponse resp;

    @Autowired
    private HttpServletRequest req;

    @Autowired
    private AdminService adminService;

    @Autowired
    private GoodService goodService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderServiceRpc orderService;

    @Autowired
    private JedisConfig jedisConfig;

    /**
     * @Description: 获取验证码
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "validateCode",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public void validateCode() throws IOException {
        Map<String, Object> validateCode = ValidateCodeUtils.getImg();
        //获取生成的验证码
        String code = String.valueOf(validateCode.get("code"));
        //把验证码存入session
        System.out.println(code);
        jedisConfig.setString(AppConstant.SESSION_VALIDATE_CODE,code);
//        req.getSession().setAttribute(AppConstant.SESSION_VALIDATE_CODE, code);
        BufferedImage image = (BufferedImage) validateCode.get("img");
        //设置响应的输出格式为图片格式
        resp.setContentType("image/jpeg");
        ImageIO.write(image, "jpeg", resp.getOutputStream());
    }

    /**
     * @Description: 登录操作
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "login",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(String userName, String pwd, String code){
        String rightCode = jedisConfig.getString(AppConstant.SESSION_VALIDATE_CODE);
        Result result = adminService.login(userName,pwd,code,rightCode);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取管理员权限信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "loginAdmin", method = RequestMethod.GET)
    public Result getLoginAdmin() {
        Result result = adminService.getLoginAdmin();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取管理员具体信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getAdminInfo", method = RequestMethod.GET)
    public Result getLoginAdminInfo() {
        Result result = adminService.getLoginAdminInfo();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 更新管理员信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "updateAdminInfo", method = RequestMethod.PUT)
    public Result getLoginAdminInfo(@RequestBody UpdateAdmin updateAdmin) {
        Result result = adminService.updateAdmin(updateAdmin);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 更新管理员密码
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "updatePwd", method = RequestMethod.PUT)
    public Result getLoginAdminInfo(@RequestBody AdminPwd adminPwd) {
        Result result = adminService.updateAdminPwd(adminPwd);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有管理员的登录信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getAdminLoginInfo", method = RequestMethod.POST)
    public Result getAdminLoginInfo() {
        Result result = adminService.getAdminLoginInfo();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有管理员的具体信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getAllAdmin", method = RequestMethod.GET)
    public Result getAllAdmin() {
        Result result = adminService.getAllAdmin();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 通过用户和添加时间获取用户的信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "admin", method = RequestMethod.POST)
    public Result getAdminByCondition(String keyword,String times) {
        System.out.println(keyword);
        Result result = adminService.getAdminByCondition(keyword,times);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取各种管理员的数量
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "admin/size", method = RequestMethod.GET)
    public Result getAdmins() {
        Result result = adminService.getAdmins();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 停用管理员
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "memberStop/{id}", method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result memberStop(@PathVariable("id") Integer id) {
        Result result = adminService.memberStop(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 启用管理员
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "memberStart/{id}", method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result memberStart(@PathVariable("id") Integer id) {
        Result result = adminService.memberStart(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 删除管理员
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "delAdmin", method = RequestMethod.DELETE,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result memberStart(String ids) {
        Result result = adminService.delAdmin(ids);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取用户的权限信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getAdminRight", method = RequestMethod.GET)
    public Result getAdminRight() {
        Result result = adminService.getAdminRights();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取用户的权限信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "batchDelAdmin", method = RequestMethod.DELETE)
    public Result batchDelAdmin(String ids) {
        Result result = adminService.batchDelAdmin(ids);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取商品分类信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getProductSort",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProductSort(){
        Result result = goodService.getAllCategorys();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "getProduct",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProduct(){
        Result result = new Result();
        try{
            List<Product> productList = goodService.getProducts();
            result.setCode(200);
            result.setData(productList);
        }catch (ShopSystemException e){
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 通过产品名称和时间获取对应商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "product",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProduct(String keyword,String times){
        Result result = goodService.getProductsByKeywordAndTime(keyword,times);
        System.out.println(result);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 停用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStop/{id}", method = RequestMethod.PUT)
    public Result productStop(@PathVariable("id") Integer id) {
        Result result = goodService.productStop(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 启用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStart/{id}", method = RequestMethod.PUT)
    public Result productStart(@PathVariable("id") Integer id) {
        Result result = goodService.productStart(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 删除商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "delProduct", method = RequestMethod.DELETE)
    public Result delProduct(String ids) {
        Result result = goodService.delProduct(ids);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "getBrands", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getBrands() {
        Result result = goodService.getBrands();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 通过品牌名，时间，是否国内品牌获取获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brand", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getBrandsByNameAndTime(String keyword,String times,String type) {
        Result result = goodService.getBrandsByNameAndTime(keyword,times,type);
        System.out.println(result);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 停用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStop/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result brandStop(@PathVariable("id") Integer id) {
        Result result = goodService.brandStop(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 启用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStart/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result brandStart(@PathVariable("id")Integer id) {
        Result result = goodService.brandStart(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 删除品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "delBrand", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delBrand(String ids) {
        Result result = goodService.delBrand(ids);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "getOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrders() {
        Result result = orderService.getOrders();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 通过订单编号和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrdersByCondition(String keyword,String times) {
        Result result = orderService.getOrdersByCondition(keyword,times);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 通过产品名称和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "refundOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getOrdersByConditions(String keyword,String times) {
        Result result = orderService.getOrdersByConditions(keyword,times);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 管理员发货操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "deliverGoods/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result deliverGoods(@PathVariable("id")Integer id) {
        Result result = orderService.deliverGoods(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 管理员退款操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "refund/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result refund(@PathVariable("id")Integer id) {
        Result result = orderService.refund(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 删除订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "delOrder", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result delOrder(@RequestParam("id") String id) {
        Result result = orderService.delOrder(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有用户信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users", method = RequestMethod.GET)
    public Result getUsers() {
        Result result = adminService.getUsers();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取用户的账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.GET)
    Result getUserRecords(){
        return userService.getUserRecords();
    }

    /**
     * @Description: 通过用户名获取账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.POST)
    Result getUserRecordsByCondition(String keyword){
        return userService.getUserRecordsByCondition(keyword);
    }

    /**
     * @Description: 获取符合当前查询条件的会员信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/keyWords", method = RequestMethod.GET)
    public Result getUsersByKeyWords(String keyWord) {
        System.out.println(keyWord);
        Result result = userService.getUsersByKeyWord(keyWord);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取符合当前查询条件的会员信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/keyWords/times", method = RequestMethod.GET)
    public Result getUsersByKeyWordsAndTimes(String keyWord,String time) {
        System.out.println(keyWord);
        System.out.println(time);
        Result result = userService.getUsersByKeyWordAndTime(keyWord,time);
        resp.setStatus(result.getCode());
        return result;
    }



    /**
     * @Description: 获取当前用户的订单积分获取记录
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/integrations", method = RequestMethod.GET)
    public Result getIntegrationHistory(Integer id) {
        Result result = orderService.getIntegrationHistory(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取当前用户的订单浏览记录
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/browses", method = RequestMethod.GET)
    public Result getBrowseHistory(Integer id) {
        Result result = orderService.getBrowseHistory(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取当前用户的订单购物记录
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/orders", method = RequestMethod.GET)
    public Result getOrderHistory(Integer id) {
        Result result = orderService.getOrderHistory(id);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 获取所有文章信息
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 21:21
     */
    @RequestMapping(value = "article",method = RequestMethod.POST)
    Result getArticle(){
        return adminService.getArticle();
    }

    /**
     * @Description: 获取所有分类信息
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 21:21
     */
    @RequestMapping(value = "sort",method = RequestMethod.POST)
    Result getSort(){
        return adminService.getSort();
    }

    /**
     * @Description: 获取所有的会员等级信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/levels", method = RequestMethod.GET)
    public Result getLevelCount() {
        Result result = adminService.getLevelCount();
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 删除会员信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users", method = RequestMethod.DELETE)
    public Result deleteUsers(String ids) {
        Result result = userService.deleteUsers(ids);
        resp.setStatus(result.getCode());
        return  result;
    }

    /**
     * @Description: 更改用户的启用状态
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users", method = RequestMethod.PUT)
    public Result updateUser(String id,String status) {
        Result result = userService.uploadUser(id,status);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 添加会员用户
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users", method = RequestMethod.POST)
    public Result addUser(@RequestBody MemberParams memberParams) {
        Result result =  userService.addUser(memberParams);
        resp.setStatus(result.getCode());
        System.out.println(memberParams);
        return result;
    }


    /**
     * @Description: 获取会员用户
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/{id}", method = RequestMethod.GET)
    public Result getUser(@PathVariable("id")String id) {
        User user =  userService.getUser(id);
        Result result = new Result();
        result.setCode(200);
        result.setData(user);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 修改会员用户信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/{id}", method = RequestMethod.PUT)
    public Result uploadUser(@PathVariable("id")String id,@RequestBody MemberParams memberParams) {
        Result result =  userService.uploadUserById(id,memberParams);
        resp.setStatus(result.getCode());
        return result;
    }

    /**
     * @Description: 清除会员用户的记录信息
     * @Author: 孙玮立
     * @Date:
     */
    @RequestMapping(value = "admins/users/shopRecords", method = RequestMethod.POST)
    public Result clearRecords(String ids) {
        Result result =  userService.clearRecords(ids);
        resp.setStatus(result.getCode());
        return result;
    }

}
