package com.sy.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.sy.dto.Result;
import com.sy.entity.Admin;
import com.sy.entity.AdminRole;
import com.sy.exception.ShopSystemException;
import com.sy.vo.AdminPwd;
import com.sy.vo.UpdateAdmin;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 管理员的服务类
 * @Author: 陈聪敏
 * @Date:
 */
public interface AdminService {
    /**
     * @Description: 登陆操作
     * @Author: 陈聪敏
     * @Date:
     */
    Result login(String userName, String pwd, String code,String rightCode);

    /**
     * @Description:  获取登录管理员权限信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result getLoginAdmin();

    /**
     * @Description:  获取登录管理员具体信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result getLoginAdminInfo();

    /**
     * @Description: 更新管理员信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result updateAdmin(UpdateAdmin updateAdmin);

    /**
     * @Description: 更新管理员密码
     * @Author: 陈聪敏
     * @Date:
     */
    Result updateAdminPwd(AdminPwd adminPwd);

    /**
     * @Description: 获取管理员登录的信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result getAdminLoginInfo();

    /**
     * @Description: 获取所有管理员的信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result getAllAdmin();

    /**
     * @Description: 通过用户和添加时间获取用户的信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "admin", method = RequestMethod.POST)
    public Result getAdminByCondition(String keyword,String times);

    /**
     * @Description: 获取各种管理员的数量
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "admin/size", method = RequestMethod.GET)
    Result getAdmins();

    /**
     * @Description: 停用管理员
     * @Author: 陈聪敏
     * @Date:
     */
    Result memberStop(Integer id);

    /**
     * @Description: 启用管理员
     * @Author: 陈聪敏
     * @Date:
     */
    Result memberStart(Integer id);

    /**
     * @Description: 删除管理员
     * @Author: 陈聪敏
     * @Date:
     */
    Result delAdmin(String ids);

    /**
     * @Description: 获取管理员的权限信息
     * @Author: 陈聪敏
     * @Date:
     */
    Result getAdminRights();

    /**
     * @Description: 群删管理员
     * @Author: 陈聪敏
     * @Date:
     */
    Result batchDelAdmin(String ids);

    /**
     * @Description: 获取所有文章信息
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 21:21
     */
    @RequestMapping(value = "article",method = RequestMethod.POST)
    Result getArticle();

    /**
     * @Description: 获取所有文章信息
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 21:21
     */
    @RequestMapping(value = "article",method = RequestMethod.POST)
    Result getSort();

    /**
     * @Description: 获取所有用户信息
     * @Author: 孙玮立
     * @Date:
     */
    Result getUsers();


    /**
     * @Description: 获取所有等级信息
     * @Author: 孙玮立
     * @Date:
     */
    Result getLevelCount();







}
