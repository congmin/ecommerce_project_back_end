package com.sy.service.rpc;

import com.sy.dto.Result;
import com.sy.entity.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description:
 * @Author: 陈聪敏
 * @Date: 2020/3/18 0018 13:18
 */
//@FeignClient("good-module")
public interface GoodServiceRpc {
    @RequestMapping(value = "categorytrees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllCategoryTrees();
    @RequestMapping(value = "products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Product getProductsByiId(@RequestParam(value = "id") Integer id);

    @RequestMapping(value = "products/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List<Product> getProducts();

    @RequestMapping(value = "products/today", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllTodayProducts();

    /**
     * @Description: 通过产品名称和时间获取对应商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "product", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result  getProductsByKeywordAndTime(@RequestParam(value = "keyword")String keyword,@RequestParam(value = "times")String times);

    /**
     * @Description:获取所有分类信息
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "categorys", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result  getAllCategorys();

    /**
     * @Description: 停用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStop/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result productStop(@RequestParam(value = "id") Integer id);

    /**
     * @Description: 启用商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "productStart/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result productStart(@RequestParam(value = "id")Integer id);

    /**
     * @Description: 删除商品
     * @Author: 陈聪敏
     * @Date:
     */
    @RequestMapping(value = "delProduct", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    Result delProduct(@RequestParam(value = "ids")String ids);

    /**
     * @Description: 获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "getBrands", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getBrands();

    /**
     * @Description: 通过品牌名，时间，是否国内品牌获取获取所有品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brand", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getBrandsByNameAndTime(@RequestParam("keyword") String keyword,@RequestParam("times")String times,@RequestParam("type")String type);

    /**
     * @Description: 停用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStop/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result brandStop(@RequestParam("id") @PathVariable("id") Integer id);

    /**
     * @Description: 启用品牌
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "brandStart/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result brandStart(@RequestParam("id")@PathVariable("id") Integer id);

    /**
     * @Description: 删除品牌信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 19:29
     */
    @RequestMapping(value = "delBrand", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    Result delBrand(@RequestParam("ids")String ids);

    /**
     * 根据商品Bid查找所有商品的种类
     * 朱天宇
     * */
    @RequestMapping(value = "brands/cid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllBrandsByCid(@RequestParam("cids")String cids);


    /**
     * 根据商品cid查找所有商品的品牌
     * 朱天宇
     * */
    @RequestMapping(value = "category/bid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllCategorysByBid(@RequestParam("bids")String bids);

    /**
     * 根据商品的品牌种类，排序，还有，价格查询结果分页
     * 朱天宇
     * */
    @RequestMapping(value = "products/conditon", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllProductsByCondtion(@RequestParam(value = "bid",required = false)Integer bid,@RequestParam(value = "cid",required = false)Integer cid,@RequestParam(value = "page",required = false)Integer page);


    /**
     * 根据商品的销量和价格综合排序
     * 朱天宇
     * */
    @RequestMapping(value = "products/sort", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllProductsByVolume(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page);


    /**
     * 根据商品的销量和价格综合排序
     * 朱天宇
     * */
    @RequestMapping(value = "products/price", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllProductsByPrice(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page);



    /**
     * 查询销量最高的product
     * 朱天宇
     * */
    @RequestMapping(value = "products/comment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getAllProductsByComment(@RequestParam(value = "ids")String ids,@RequestParam(value = "page",required = false)Integer page);

    /**
     * @Description: 获取产品的口味，包装,评价等具体信息
     * @Author: 孙玮立
     * @Date: 2020/3/16  16:29
     */
    @RequestMapping(value = "products/{id}/flavors/packages/evaluates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getProductInfo(@RequestParam("token") String token,@RequestParam("id")  @PathVariable("id") Integer id);

}
