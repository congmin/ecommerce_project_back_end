package com.sy.service.rpc;

import com.sy.dto.Result;
import com.sy.entity.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@FeignClient("order-module")
public interface OrderServiceRpc {
    //    @RequestMapping(value = "orders/items/springcloud", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    Result getOrderItmesSpringcloud(@RequestParam(value = "oid") Integer oid, @RequestParam(value = "token") String token);
    @RequestMapping(value = "orders/order", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Order getOrderByid(@RequestParam(value = "oid") Integer oid);

    /**
     * @Description: 获取所有的订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 21:59
     */
    @RequestMapping(value = "getOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getOrders();

    /**
     * @Description: 通过订单编号和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getOrdersByCondition(@RequestParam("keyword") String keyword,@RequestParam("times")String times);

    /**
     * @Description: 通过产品名称和时间获取所有订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:04
     */
    @RequestMapping(value = "refundOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getOrdersByConditions(@RequestParam("keyword") String keyword,@RequestParam("times")String times);

    /**
     * @Description: 管理员发货操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "deliverGoods/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result deliverGoods(@RequestParam("id")@PathVariable("id")Integer id);

    /**
     * @Description: 管理员退款操作
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "refund/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    Result refund(@RequestParam("id")@PathVariable("id")Integer id);

    /**
     * @Description: 删除订单信息
     * @Author: 陈聪敏
     * @Date: 2020/3/15 0015 22:07
     */
    @RequestMapping(value = "delOrder", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    Result delOrder(@RequestParam("ids")String ids);

    /**
     * @Description: 获取当前用户的订单积分获取记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/integrations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getIntegrationHistory(@RequestParam("id")Integer id);

    /**
     * @Description: 获取当前用户的订单浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/browses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getBrowseHistory(@RequestParam("id")Integer id);

    /**
     * @Description: 获取当前用户的订单购物记录
     * @Author: 孙玮立
     * @Date: 2020/3/19 16:29
     */
    @RequestMapping(value = "orders/users/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Result getOrderHistory(@RequestParam("id")Integer id);
}
