package com.sy.impl;

import com.sy.config.RedisClient;
import com.sy.constant.AppConstant;
import com.sy.dto.Result;
import com.sy.entity.DealOrder;
import com.sy.entity.Order;
import com.sy.entity.OrderProductRef;
import com.sy.entity.User;
import com.sy.exception.NotFoundException;
import com.sy.mapper.DealOrderMapper;
import com.sy.mapper.OrderMapper;
import com.sy.mapper.OrderProductRefMapper;
import com.sy.mapper.UserMapper;
import com.sy.service.PayService;
import com.sy.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Service
public class PayServiceImpl implements PayService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DealOrderMapper dealOrderMapper;
    @Autowired
    private RedisClient redisClient;
    @Autowired
    private OrderProductRefMapper orderProductRefMapper;

    @Autowired
    private UserService userService;
    @Override
    @Transactional
    public Result commitPay(Integer oid, String token, Integer aid,String message,String password) {
        Result result=new Result();
        try {


            User user = userService.LoginUser(token);
            String payWordByUid = userMapper.getPayWordByUid(user.getId());
            if (!StringUtils.equals(payWordByUid, password)) {
                throw new NotFoundException("密码不正确");
            }
            if (message==null){

            }
            Integer uid = user.getId();
            orderMapper.commitOrder(uid, oid, message, aid);//提交订单
            Order order = orderMapper.selectById(oid);//插入dealorder表
            userMapper.updateUserAfterCommit(uid, order.getTotalprice(), new Date(System.currentTimeMillis()));//修改user的积分属性和相关的属性
            String redisCartKey = AppConstant.USER_CART_REDIS_PREFIX + uid;
            List<OrderProductRef> orderProductRefs = orderProductRefMapper.getOrderProductRefs(order.getId());
            for (OrderProductRef orderProductRef : orderProductRefs) {
                redisClient.hDel(redisCartKey, String.valueOf(orderProductRef.getPid()));    //删除购物车
            }
            DealOrder dealOrder = new DealOrder();
            dealOrder.setId(order.getId());
            dealOrder.setOrderno(order.getOrderno());
            dealOrder.setOrderprice(order.getTotalprice());
            dealOrder.setOrdertime(order.getOrdertime());
            dealOrderMapper.insert(dealOrder);
            result.setCode(200);
        }catch (NotFoundException e){
            result.setError(e.getMessage());
            result.setCode(404);
        }
        catch (Exception e){
            e.printStackTrace();
            result.setError(e.getMessage());
            result.setCode(500);
        }
        return result;
    }



}
