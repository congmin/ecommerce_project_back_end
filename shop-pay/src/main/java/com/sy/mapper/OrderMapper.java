package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface OrderMapper extends BaseMapper<Order> {
    @Update("update  ordert set status=1 , message=#{message} ,addressid=#{addressid}   where  uid=#{uid} and id=#{oid}")
    Integer commitOrder(@Param(value = "uid") Integer uid,@Param(value = "oid")Integer oid,@Param(value = "message")String message,@Param(value = "addressid")Integer addressid);

   @Select(value = "select totalprice from ordert where id=#{oid}")
    BigDecimal getTotalPrice(@Param(value = "oid") Integer oid);
}
