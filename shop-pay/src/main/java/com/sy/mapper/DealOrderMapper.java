package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.DealOrder;
import org.apache.ibatis.annotations.Insert;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface DealOrderMapper extends BaseMapper<DealOrder> {

}
