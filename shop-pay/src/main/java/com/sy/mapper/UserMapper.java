package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
//    //latestConsumption,comsumptionTime
public interface UserMapper extends BaseMapper<User> {
    @Update(value = "update user set integral=integral+10,latestconsumption=#{lat},comsumptiontime=#{time} where id=#{uid}")
    Integer updateUserAfterCommit(@Param(value = "uid") Integer uid, @Param(value = "lat")BigDecimal lat, @Param(value = "time") Date time);
    @Select(value = "select paypwd from user where id=#{uid}")
    String getPayWordByUid(@Param(value = "uid") Integer uid);
}
