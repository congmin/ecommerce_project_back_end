package com.sy.controller;

import com.sy.dto.Result;
import com.sy.mapper.OrderMapper;
import com.sy.service.PayService;
import com.sy.utils.PaymentUtil;
import feign.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RestController
public class PayController {
    @Autowired
    private HttpServletResponse response;
    @Autowired
    private PayService payService;

    @Autowired
    private OrderMapper orderMapper;
    @RequestMapping(value = "commitpay",method = RequestMethod.GET)
    public Result CommitPay(@RequestParam(value = "oid") Integer oid, @RequestHeader(value = "token") String token, @RequestParam(value = "aid") Integer aid, @RequestParam(value = "message") String message, @Param(value = "password")String password){
        Result result = payService.commitPay(oid, token, aid, message, password);
        System.out.println(result);
        return result;
    }

    /**
     * 调用易宝支付发起支付操作
     */
    @RequestMapping("pay")
    public void pay(String oid,String pd_FrpId) {
        // 发起支付
        // 2.完成支付功能
        // 获得 支付必须基本数据
        //String orderid = ParamUtils.getParam("oid");
        // String money = order.getTotal()+"";
        //TODO:不要乱改！！！！！
        //TODO:这里应该通过OrderService中的方法将价格重新计算一下！

        BigDecimal totalPrice = orderMapper.getTotalPrice(Integer.parseInt(oid));
        String money =totalPrice.toString();
        System.out.println(money);
        // 银行
        //String pd_FrpId = ParamUtils.getParam("pd_FrpId");
        // 发给支付公司需要哪些数据
        String p0_Cmd = "Buy";
        //用户注册并认证后在后台系统可以得到的一个标识
        String p1_MerId = "10012442782";
        //String p2_Order = orderid;
        String p2_Order = oid;
        //TODO:支付金额
        String p3_Amt = money;
        String p4_Cur = "CNY";
        String p5_Pid = "";
        String p6_Pcat = "";
        String p7_Pdesc = "";
        // 支付成功回调地址 ---- 第三方支付公司会访问、用户访问
        // 这个路径可以是一个网页路径也可以是一个Servlet的处理路径
        String p8_Url = "http://localhost:8992/toIndex";
        String p9_SAF = "";
        String pa_MP = "";
        String pr_NeedResponse = "1";
        // 加密hmac 需要密钥 用户注册并认证后在后台系统可以得到的一个秘钥
        String keyValue = "mP42238826nuW64r7yh26DGK34o2L2m81L25RG32lD7Lo1058A7iJ28at6QS";
        String hmac = PaymentUtil.buildHmac(p0_Cmd, p1_MerId, p2_Order, p3_Amt, p4_Cur, p5_Pid, p6_Pcat, p7_Pdesc,
                p8_Url, p9_SAF, pa_MP, pd_FrpId, pr_NeedResponse, keyValue);
        String url = "https://www.yeepay.com/app-merchant-proxy/node?pd_FrpId=" + pd_FrpId + "&p0_Cmd=" + p0_Cmd
                + "&p1_MerId=" + p1_MerId + "&p2_Order=" + p2_Order + "&p3_Amt=" + p3_Amt + "&p4_Cur=" + p4_Cur
                + "&p5_Pid=" + p5_Pid + "&p6_Pcat=" + p6_Pcat + "&p7_Pdesc=" + p7_Pdesc + "&p8_Url=" + p8_Url
                + "&p9_SAF=" + p9_SAF + "&pa_MP=" + pa_MP + "&pr_NeedResponse=" + pr_NeedResponse + "&hmac=" + hmac;
        // 重定向到第三方支付平台
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("toIndex")
    public void toIndex(HttpServletResponse response) {

        //TODO:订单状态要设置为已经支付
        //跳转到支付后的页面
        try {
            response.sendRedirect("http://127.0.0.1:8848/ecommerce_project_front_end/shopsystemweb/one/home/home3.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
