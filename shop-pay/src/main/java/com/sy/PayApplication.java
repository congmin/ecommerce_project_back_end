package com.sy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@SpringBootApplication
@MapperScan("com.sy.mapper")
@EnableFeignClients
@EnableEurekaClient
@EnableTransactionManagement
public class PayApplication
{
    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class,args);
    }
}
