package com.sy.service;

import com.sy.dto.Result;
import com.sy.entity.Product;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

public interface PayService {
    Result  commitPay (@RequestParam(value = "oid") Integer oid, @RequestHeader(value = "token") String  token, @RequestParam(value = "aid") Integer aid, @RequestParam(value = "message") String message, @Param(value = "password")String password);
}

