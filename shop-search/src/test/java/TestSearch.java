
import com.sy.GoodSearchApplication;


import com.sy.index.BrandIndex;
import com.sy.index.CategoryIndex;
import com.sy.index.ProductIndex;
import com.sy.repository.BrandRepository;
import com.sy.repository.CategoryRepository;
import com.sy.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GoodSearchApplication.class)
public class TestSearch {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    BrandRepository brandRepository;
//    @Autowired
//    private ElasticsearchRestTemplate elasticsearchRestTemplate;
    @Autowired
    private ProductRepository productRepository;

    @Test
    public void test(){
//        System.out.println("成功了");
//       // jedisConfig.setString(AppConstant.AllGoodInfoExceptPicture_RedisKey, JSON.toJSONString(goodService.getProducts()));
//        List<ProductIndex> allByCid = productRepository.findAllByCid(29);
//        System.out.println(allByCid);
        CategoryIndex byCid = categoryRepository.findByCid(29);
        BrandIndex byBid = brandRepository.findByBid(1);
        System.out.println(byCid);
        System.out.println(byBid);
        List<ProductIndex> allByCid = productRepository.findAllByCid(29);
        System.out.println(allByCid);
        List<ProductIndex> allByBid = productRepository.findAllByBid(1);
        System.out.println(allByBid);
        //   List<CategoryIndex> hh = categoryRepository.findCategoryIndicesByName("法");
        //System.out.println(hh);
//        Optional<CategoryIndex> byId = categoryRepository.findById(1);
//        List<CategoryIndex> byCid = categoryRepository.findAllByName("法式软面包");
//        System.out.println(byCid);
//        List<Article> 睛不好使 = articleRepository.findAllByContent("睛");
//        System.out.println(睛不好使);
       // List<ProductIndex> 红富士 = productRepository.findAllByProductName("洽洽焦糖");
        //System.out.println(红富士);
//        StringQuery query=new StringQuery("睛不好使");
//        List<Article> articles = elasticsearchRestTemplate.queryForList(query, Article.class);
//        System.out.println(articles);
        //    categoryRepository.findCategoryIndicesByName("")
//        CategoryIndex categoryIndex=new CategoryIndex();
//        categoryIndex.setCid(1);
//        categoryIndex.setParent(0);
//        categoryIndex.setName("点心/蛋糕");
//        categoryIndex.setIsparent(1);
//        categoryRepository.save(categoryIndex);
    }
}
