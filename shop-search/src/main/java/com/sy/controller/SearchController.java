package com.sy.controller;

import com.sy.dto.Result;

import com.sy.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@RestController
public class SearchController {
    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
   public Result getGoodListByNameFromElasticsearch (@RequestParam(value = "goodname") String goodname){
        return  searchService.getGoodListByNameFromElasticsearch(goodname);
    }

}
