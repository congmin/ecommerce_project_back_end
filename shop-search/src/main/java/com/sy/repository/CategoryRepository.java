package com.sy.repository;



import com.sy.index.CategoryIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface CategoryRepository extends ElasticsearchRepository<CategoryIndex, String> {
       List<CategoryIndex>  findAllByName(String name);
       Integer deleteByCid(Integer cid);
       CategoryIndex findByCid(Integer cid);
       List<CategoryIndex> findAllByParent(Integer parent);
 }
