package com.sy.repository;

import com.sy.index.BrandIndex;
import com.sy.index.ProductIndex;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface ProductRepository extends ElasticsearchRepository<ProductIndex,String> {
    List<ProductIndex> findAllByProductnameAndStatus(String name,Integer status);
   Integer deleteByPid(Integer pid);
    ProductIndex findByPid(Integer pid);
    List<ProductIndex> findAllByBid(Integer bid);
    List<ProductIndex> findAllByBidAndStatus(Integer bid,Integer status);
    List<ProductIndex> findAllByCid(Integer cid);
    Integer deleteByBid(Integer bid);
}
