package com.sy.repository;

import com.sy.index.BrandIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface BrandRepository extends ElasticsearchRepository<BrandIndex, String> {
    List<BrandIndex> findAllByBrandnameAndStatus(String name,Integer status );
    Integer deleteByBid(Integer bid);
    BrandIndex findByBid(Integer bid);
}
