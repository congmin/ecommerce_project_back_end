package com.sy.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.sy.dto.Result;
import com.sy.index.BrandIndex;
import com.sy.index.CategoryIndex;
import com.sy.index.ProductIndex;
import com.sy.repository.BrandRepository;
import com.sy.repository.CategoryRepository;
import com.sy.repository.ProductRepository;

import com.sy.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BrandRepository brandRepository;

    /**
     * saerchename是传过来的名字
     * allc和allp还有allb是ES库里的的搜索结果
     * 返回结果是三个集合分别对应着product表的 Category表的，和Brand表的
     **/
    @Override
    public Result getGoodListByNameFromElasticsearch(String searchname) {
        Result result=new Result();
        try {
        if (searchname==null||searchname.equals("")){
             throw new Exception("参数有误");
        }
        List<CategoryIndex> allc = categoryRepository.findAllByName(searchname);
        List<ProductIndex>  allp = productRepository.findAllByProductnameAndStatus(searchname,1);
        List<BrandIndex> allb = brandRepository.findAllByBrandnameAndStatus(searchname,1);
        List<Object> list=new ArrayList<>();
        List<CategoryIndex> newlistc=null;
        List<CategoryIndex> newlistc1=new ArrayList<>();

            if (CollectionUtils.isEmpty(allc)&CollectionUtils.isEmpty(allp)&CollectionUtils.isEmpty(allb)){
                throw new Exception("无数据");
            }
            else {
                if (searchname.equals("水果")){
                    List<CategoryIndex> allByParent = categoryRepository.findAllByParent(21);
                    for (CategoryIndex c:allByParent){
                        List<CategoryIndex> allByParent1 = categoryRepository.findAllByParent(c.getCid());
                        newlistc=allByParent1;
                    }
                    list.add(newlistc);
                }  else if (searchname.equals("坚果")){
                    List<CategoryIndex> allByParent = categoryRepository.findAllByParent(10);
                    for (CategoryIndex c:allByParent){
                        List<CategoryIndex> allByParent1 = categoryRepository.findAllByParent(c.getCid());
                        newlistc=allByParent1;
                    }
                    list.add(newlistc);
                }

                else {
                    for (CategoryIndex categoryIndex :allc){
                        if (categoryIndex.getIsparent().equals(0)){
                            newlistc1.add(categoryIndex);

                        }
                    }
                    list.add(newlistc1);
                }

               list.add(allb);
               list.add(allp);
               result.setData(list);
               result.setCode(200);
            }

        }catch (Exception e){
            e.printStackTrace();
            result.setCode(404);
            result.setError(e.getMessage());
        }return result;

    }

    @Override
    public Integer deleteByPid(Integer pid) {
     return productRepository.deleteByPid(pid);
    }

    @Override
    public ProductIndex findByPid(Integer pid) {
        return productRepository.findByPid(pid);
    }

    @Override
    public Integer deleteByCid(Integer cid) {
        return categoryRepository.deleteByCid(cid);
    }

    @Override
    public CategoryIndex findByCid(Integer cid) {
        return categoryRepository.findByCid(cid);
    }

    @Override
    public Integer deleteByBid(Integer bid) {
        return brandRepository.deleteByBid(bid);
    }

    @Override
    public BrandIndex findByBid(Integer bid) {
        return brandRepository.findByBid(bid);
    }


    /***
     * 存贮3种对象到ES传入3种index对象
     *
     *
     * **/
    @Override
    public Object save(Object object) {
        Object o=null;
        if (object instanceof ProductIndex){
            ProductIndex productIndex=(ProductIndex)object;
            ProductIndex save = productRepository.save(productIndex);
            o=save;
        }else  if (object instanceof CategoryIndex){
            CategoryIndex categoryIndex=(CategoryIndex)object;
            CategoryIndex save = categoryRepository.save(categoryIndex);
            o=save;
        } else if (object instanceof BrandIndex) {
            BrandIndex brandIndex=(BrandIndex)object;
            BrandIndex save = brandRepository.save(brandIndex);
            o=save;
        }
        if (o!=null){
            return o;
        }else {
            return "成功了";
        }

    }

    @Override
    public List<ProductIndex> findAllByBid(Integer bid) {
        return productRepository.findAllByBid(bid);
    }

    @Override
    public List<ProductIndex> findAllByCid(Integer cid) {
        return productRepository.findAllByCid(cid);
    }

    @Override
    public Integer deleteProductByBid(Integer bid) {
        return productRepository.deleteByBid(bid);
    }


}
