package com.sy;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.sy.repository")
@EnableDubbo
public class GoodSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(GoodSearchApplication.class,args);
    }
}
