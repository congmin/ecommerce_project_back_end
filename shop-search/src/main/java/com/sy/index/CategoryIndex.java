package com.sy.index;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data//category_index
@Document(indexName = "category_index", createIndex = true)
public class CategoryIndex implements Serializable {
    private static final long serialVersionUID = 654647457533214L;
    @Id
    private String id;


    @Field(type = FieldType.Integer, store = true)
    private Integer cid;

    @Field(type = FieldType.Integer, store = true)
    private Integer parent;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String name;
    @Field(type = FieldType.Long, store = true, analyzer = "ik_max_word")
    private Integer isparent;




}
