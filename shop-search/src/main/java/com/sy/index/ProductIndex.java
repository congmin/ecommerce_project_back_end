package com.sy.index;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.sql.Date;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@Data
@Document(indexName = "product_index", createIndex = false)
public class ProductIndex implements Serializable {
    private static final long serialVersionUID = 876315234123213L;
    @Id
    private String id;

    @Field(type = FieldType.Long, store = true)
    private Long pid;


    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String productname;

    @Field(type = FieldType.Long, store = true)
    private Long bid;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String originalprice;

    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    private String country;

    @Field(type = FieldType.Integer, store = true,analyzer = "ik_max_word")
    private Integer cid;
    @Field(type = FieldType.Integer, store = true,analyzer = "ik_max_word")
    private String productpicture;

    @Field(type = FieldType.Integer, store = true)
    private Integer status;
}
/**
 * @Data
 * @Document(indexName = "category_index", createIndex = false)
 * public class CategoryIndex {
 *     @Id
 *     private String id;
 *
 *     @Field(type = FieldType.Integer, store = true)
 *     private Integer parent;
 *     @Field(type = FieldType.Text, store = true, analyzer = "ik_max_word")
 *     private String name;
 *     @Field(type = FieldType.Integer, store = true, analyzer = "ik_max_word")
 *     private Integer isparent;
 *
 *
 * }
 *
 *
 *
 * **/