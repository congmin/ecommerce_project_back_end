package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Flavor;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


public interface FlavorMapper extends BaseMapper<Flavor> {
    @Select(value = "select flavor from flavor where id=#{fid}")
    public String  getFlavorName(@Param(value = "fid") Integer fid);
}
