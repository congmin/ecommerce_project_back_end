package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Packaging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Description:
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
public interface PackagingMapper extends BaseMapper<Packaging> {
    @Select(value = "select packaging from packaging where id=#{fid}")
    public String  getPackagname(@Param(value = "fid") Integer fid);
}
