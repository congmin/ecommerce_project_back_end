package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:用户相关mapper层
 * @Author: 董星奇
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
