package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Level;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/13 20:15
 */
@Mapper
public interface LevelMapper extends BaseMapper<Level> {
}
