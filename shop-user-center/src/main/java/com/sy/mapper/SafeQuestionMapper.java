package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.SafeQuestion;
import org.apache.ibatis.annotations.Mapper;

/**
 * 安全问题mapper接口
 * @author 董星奇
 */
@Mapper
public interface SafeQuestionMapper extends BaseMapper<SafeQuestion> {
}
