package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.City;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:市mapper层
 * @Author: 董星奇
 */
@Mapper
public interface CityMapper extends BaseMapper<City> {
}
