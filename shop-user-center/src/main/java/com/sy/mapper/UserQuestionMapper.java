package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.UserQuestion;
import org.apache.ibatis.annotations.Mapper;

/**用户安全问题关系mapper接口
 * @author 董星奇
 */
@Mapper
public interface UserQuestionMapper extends BaseMapper<UserQuestion> {
}
