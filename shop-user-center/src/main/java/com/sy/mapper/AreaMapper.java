package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Area;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:区mapper层
 * @Author: 董星奇
 */
@Mapper
public interface AreaMapper extends BaseMapper<Area> {
}
