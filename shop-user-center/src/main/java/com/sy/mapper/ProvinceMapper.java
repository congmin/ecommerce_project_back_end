package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.Province;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:省份mapper层
 * @Author: 董星奇
 */
@Mapper
public interface ProvinceMapper extends BaseMapper<Province> {
}
