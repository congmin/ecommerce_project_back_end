package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.History;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/17 0:11
 */
public interface HistoryMapper extends BaseMapper<History> {


}
