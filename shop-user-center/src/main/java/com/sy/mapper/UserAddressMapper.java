package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.UserAddress;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户地址关联表mapper接口
 * @Author: 董星奇
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {
}
