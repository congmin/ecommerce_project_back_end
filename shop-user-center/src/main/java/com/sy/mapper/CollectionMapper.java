package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.entity.CollectionInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description：
 * @Author: 孙玮立
 * @Date: 2020/3/21 21:28
 */
@Mapper
public interface CollectionMapper extends BaseMapper<CollectionInfo> {
}
