package com.sy.controller;

import com.sy.constant.AppConstant;
import com.sy.dto.Result;
import com.sy.entity.*;
import com.sy.service.SafeQuestionService;
import com.sy.service.UserService;
import com.sy.utils.HttpUtils;
import com.sy.vo.MemberParams;
import com.sy.vo.ReceiverParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;


/**
 * @Description:用户相关控制层接口
 * @Author:董星奇
 */
@RestController
public class UserController {
    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest req;

    @Autowired
    private HttpServletResponse res;

    @Autowired
    private SafeQuestionService safeQuestionService;

    /**
     * 邮箱注册
     *
     * @param email          邮箱
     * @param userName       用户名
     * @param emailpassword  密码
     * @param passwordRepeat 确认密码
     * @Author:董星奇
     */
    @RequestMapping(value = "registByEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result registByEmail(@RequestParam("email") String email, @RequestParam("userName") String userName, @RequestParam("emailpassword") String emailpassword, @RequestParam("passwordRepeat") String passwordRepeat) {
        Result result = userService.registByEmail(email, userName, emailpassword, passwordRepeat);
        return result;
    }

    /**
     * 手机发送验证码
     *
     * @param phone 手机号码
     * @Author:董星奇
     */
    @RequestMapping(value = "sendMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result sendMessage(@RequestParam("phone") String phone) {
        Result result = userService.sendMessage(phone);
        HttpUtils.setSessionAttribute(AppConstant.SHORT_MESSAGE_CODE_SESSION_NAME, result.getData(), req);
        return result;
    }

    /**
     * 支付密码时手机发送验证码
     *
     * @param phone 手机号码
     * @Author:董星奇
     */
    @RequestMapping(value = "sendMessage1", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result PaypwdsendMessage(@RequestParam("phone") String phone) {
        Result result = userService.sendMessage(phone);
        HttpUtils.setSessionAttribute(AppConstant.PAYPWD_MESSAGE_CODE, result.getData(), req);
        return result;
    }
    /**
     * 原来的手机发送验证码
     *
     * @param phone 手机号码
     * @Author:董星奇
     */
    @RequestMapping(value = "sendMessage2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result oldPhonesendMessage(@RequestParam("phone") String phone) {
        Result result = userService.sendMessage(phone);
        HttpUtils.setSessionAttribute(AppConstant.OLD_PHONE_CODE, result.getData(), req);
        return result;
    }
    /**
     * 新的手机发送验证码
     * @param phone 手机号码
     * @Author:董星奇
     */
    @RequestMapping(value = "sendMessage3", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result newPhonesendMessage(@RequestParam("phone") String phone) {
        Result result = userService.sendMessage(phone);
        HttpUtils.setSessionAttribute(AppConstant.NEW_PHONE_CODE, result.getData(), req);
        return result;
    }
    /**
     * 新的手机发送验证码
     * @param phone 手机号码
     * @Author:董星奇
     */
    @RequestMapping(value = "sendMessage4", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result emailsendMessage(@RequestParam("phone") String phone) {
        Result result = userService.sendMessage(phone);
        HttpUtils.setSessionAttribute(AppConstant.EMAIL_CODE, result.getData(), req);
        return result;
    }
    /**
     * @param phone               手机号码
     * @param code                输入验证码
     * @param phonepassword       输入密码
     * @param phonepasswordRepeat 确认密码
     * @param userName            用户名
     * @Author:董星奇
     */
    @RequestMapping(value = "registByPhone", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result registByPhone(@RequestParam("phone") String phone, @RequestParam("code") String code, @RequestParam("phonepassword") String phonepassword, @RequestParam("phonepasswordRepeat") String phonepasswordRepeat, @RequestParam("userName") String userName) {
        String codetoken = String.valueOf(HttpUtils.getSessionAttribute(AppConstant.SHORT_MESSAGE_CODE_SESSION_NAME, req));
        Result result = userService.registByPhone(phone, code, phonepassword, phonepasswordRepeat, userName, codetoken);
        return result;
    }

    /**
     * 登录
     * @param message  账号 ：手机号码   邮箱 姓名
     * @param password 密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(@RequestParam("message") String message, @RequestParam("password") String password) {
        Result result = userService.login(message, password);
        return result;
    }

    /**
     * 获取当前登录用户
     *
     * @param token token
     * @Author: 董星奇
     */
    @RequestMapping(value = "loginUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public User LoginUser(@RequestParam("token") String token) {
        User user = userService.LoginUser(token);
        return user;
    }

    /**
     * 忘记密码
     *
     * @param forgetMessage 邮箱或者是手机号码
     * @Author: 董星奇
     */
    @RequestMapping(value = "forget", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result forgetPassword(@RequestParam("forgetMessage") String forgetMessage) {
        System.out.println(forgetMessage);
        Result result = userService.forgetPassword(forgetMessage);
        return result;
    }

    /**
     * 个人中心资料修改
     *
     * @param id       当前登录用户的id
     * @param nickname 昵称
     * @param name     名字
     * @param sex      性别
     * @param birthday 生日
     * @param phone    手机号码
     * @param email    邮箱
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/{id}", method = RequestMethod.POST)
    public Result updateUserInfo(@PathVariable("id") Integer id, @RequestParam("nickname") String nickname, @RequestParam("name") String name, @RequestParam("sex") Integer sex, @RequestParam("birthday") String birthday, @RequestParam("phone") String phone, @RequestParam("email") String email) {
        System.out.println(id + nickname + birthday);
        Result result = userService.updateUserInfo(id, nickname, name, sex, birthday, phone, email);
        return result;
    }


    /**
     * 修改用户密码
     *
     * @param id         当前用户id
     * @param oldPwd     原密码
     * @param newPwd     新密码
     * @param confirmPwd 确认密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/user", method = RequestMethod.POST)
    public Result updateUserPwd(@RequestParam("id") Integer id, @RequestParam("oldPwd") String oldPwd, @RequestParam("newPwd") String newPwd, @RequestParam("confirmPwd") String confirmPwd) {
        Result result = userService.updateUserPwd(id, oldPwd, newPwd, confirmPwd);
        return result;
    }

    /**
     * 设置支付密码
     * @param id            当前用户id
     * @param code          输入的验证码
     * @param payPwd        支付密码
     * @param confirmPayPwd 确认支付密码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/user/user", method = RequestMethod.POST)
    public Result setUPayPwd(@RequestParam("id") Integer id, @RequestParam("code") String code, @RequestParam("payPwd") String payPwd, @RequestParam("confirmPayPwd") String confirmPayPwd) {
        String phonecode= String.valueOf(HttpUtils.getSessionAttribute(AppConstant.PAYPWD_MESSAGE_CODE,req));
        Result result=userService.setUPayPwd(id,code,payPwd,confirmPayPwd,phonecode);
        return result;
    }

    /**
     * 更改手机号码
     * @param id 当前用户id
     * @param phone 旧的手机号码
     * @param oldcode 旧手机输入的验证码
     * @param bindPhone 新绑定的手机
     * @param code  新手机输入的验证码
     * @Author: 董星奇
     */
    @RequestMapping(value = "phone",method = RequestMethod.POST)
    public Result updatePhone(@RequestParam("id") Integer id,@RequestParam("phone") String phone,@RequestParam("oldcode") String oldcode,@RequestParam("bindPhone") String bindPhone,@RequestParam("code") String code){
        String oldSessionCode= String.valueOf(HttpUtils.getSessionAttribute(AppConstant.OLD_PHONE_CODE,req));
        String newSessionCode= String.valueOf(HttpUtils.getSessionAttribute(AppConstant.NEW_PHONE_CODE,req));
        Result result=userService.updatePhone(id,phone,oldcode,bindPhone,code,oldSessionCode,newSessionCode);
        return result;
    }

    /**
     * 绑定邮箱操作
     * @param id 当前用户的id
     * @param email 邮箱
     * @param code 输入的验证码
     * @Author: 董星奇
     */
    @RequestMapping(value = "user/users",method = RequestMethod.POST)
    public Result bindEmail(@RequestParam("id") Integer id,@RequestParam("email") String email,@RequestParam("code") String code){
        String sessionCode= String.valueOf(HttpUtils.getSessionAttribute(AppConstant.EMAIL_CODE,req));
        Result result=userService.bindEmail(id,email,code,sessionCode);
        return result;
    }

    /**
     * 实名认证
     * @param id 当前用户id
     *  @param phone 手机号码
     * @param name 真实姓名
     * @param card 身份证号码
     * @Author: 董星奇
     */
    @RequestMapping(value = "users/users",method = RequestMethod.POST)
    public Result realName(@RequestParam("id") Integer id,@RequestParam("phone") String phone,@RequestParam("name") String name,@RequestParam("card") String card){
        Result result=userService.realName(id, phone, name, card);
        return result;
    }

    /**
     * 获取所有安全问题
     * @Author: 董星奇
     */
    @RequestMapping(value = "safequestions",method =RequestMethod.GET)
    public Result getAllSafeQuestions(){
        Result result=safeQuestionService.allQuestions();
        return result;
    }

    /**
     * 用户填写安全问题
     * @param id 当前用户id
     * @param questionId1 问题1id
     * @param answer1 问题1答案
     * @param questionId2 问题2id
     * @param answer2 问题2答案
     * @Author: 董星奇
     */
    @RequestMapping(value = "safequestion",method = RequestMethod.POST)
    public Result addSafeQuestions(@RequestParam("id") Integer id,@RequestParam("questionId1") Integer questionId1,@RequestParam("answer1") String answer1,@RequestParam("questionId2") Integer questionId2,@RequestParam("answer2") String answer2){
        Result result=userService.addSafeQuestions(id, questionId1, answer1, questionId2, answer2);
        return result;
    }

    /**
     * 获取所有省份
     * @Author: 董星奇
     */
    @RequestMapping(value = "provinces",method = RequestMethod.GET)
    public List<Province> getAllProvinces(){
        List<Province> provinceList=userService.getAllProvinces();
        return provinceList;
    }

    /**
     * 获取选中省份的所有城市
     * @Author: 董星奇
     */
    @RequestMapping(value = "cities",method = RequestMethod.POST)
    public List<City> getCitiesByProvinceid(@RequestParam("provinceid") Integer provinceid){
        List<City> cityList=userService.getCitiesByProvinceid(provinceid);
        return  cityList;
    }

    /**
     * 获取所有区县
     * @Author: 董星奇
     */
    @RequestMapping(value = "areas",method = RequestMethod.POST)
    public List<Area> getAreasByCityid(@RequestParam("cityid") Integer cityid){
        List<Area> areaList=userService.getAreasByCityid(cityid);
        return areaList;
    }

    /**
     * 用户新增收货地址
     * @param receiverParam 收货地址实体类
     * @Author: 董星奇
     */
    @RequestMapping(value = "location",method = RequestMethod.POST)
    public Result addReceiverAddress(@RequestBody ReceiverParam receiverParam){
        Result result=userService.addReceiverAddress(receiverParam);
        return result;
    }

    /**
     * 获取当前用户的收货地址
     * @param token token
     * @Author: 董星奇
     */
    @RequestMapping(value = "locations",method = RequestMethod.POST)
    public List<Address> getAllAddressesByUserId(@RequestParam("token") String token){
        List<Address> addressList=userService.getAllAddressesByUserId(token);
        return addressList;
    }

    /**
     * @Author 孙玮立
     * 获取所有会员信息
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.GET)
    public Result getUsers(){
        return userService.getUsers();
    }


    /**
     * @Author 孙玮立
     * 获取会员等级信息
     * @return
     */
    @RequestMapping(value = "users/levels",method = RequestMethod.GET)
    public Result getLevelCount(){
        return userService.getLevelCount();
    }

    /**
     *  @Author 孙玮立
     * 删除用户信息
     * @param ids
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.DELETE)
    public Result delUsers(@RequestParam("ids")String ids){
        return userService.deleteUsers(ids);
    }


    /**
     * @Author 孙玮立
     * 更改会员状态
     * @param id 会员编号
     * @param status 会员状态
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.PUT)
    Result uploadUser(@RequestParam("id")String id,@RequestParam("status")String status){
        return userService.uploadUser(id,status);
    }

    /**
     * @Author 孙玮立
     * 添加会员用户信息
     * @param memberParams 会员信息
     * @return
     */
    @RequestMapping(value = "users",method = RequestMethod.POST)
    Result addUser(@RequestBody MemberParams memberParams){
        return userService.addUser(memberParams);
    }


    /**
     * @Author 孙玮立
     * 获得会员用户信息
     * @param id 会员编号
     * @return
     */
    @RequestMapping(value = "users/{id}",method = RequestMethod.GET)
    User getUser(@RequestParam("id")@PathVariable("id")String id){
        return userService.getUser(id);
    }

    /**
     * 获取符合查询条件的用户信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users/keyWords",method = RequestMethod.GET)
    Result getUsersByKeyWord(@RequestParam("keyWord")String keyWord){
        return userService.getUsersByKeyWord(keyWord);
    }

    /**
     * 获取符合查询条件的用户信息
     * @Author 孙玮立
     * @return
     */
    @RequestMapping(value = "users/keyWords/times",method = RequestMethod.GET)
    Result getUsersByKeyWordAndTime(@RequestParam("keyWord")String keyWord,@RequestParam("time")String time){
        return userService.getUsersByKeyWordAndTime(keyWord,time);
    }

    /**
     * @Author 孙玮立
     * 修改会员用户信息
     * @param id 会员编号
     * @return
     */
    @RequestMapping(value = "users/{id}",method = RequestMethod.PUT)
    Result uploadUserById(@RequestParam("id")@PathVariable("id")String id,@RequestBody MemberParams memberParams){
        return userService.uploadUserById(id,memberParams);
    }


    /**
     * @Author 孙玮立
     * 清除会员用户的购物记录
     * @param ids 会员编号
     * @return
     */
    @RequestMapping(value = "users/shopRecords",method = RequestMethod.POST)
    Result clearRecords(@RequestParam("ids")String ids){
        return userService.clearRecords(ids);
    }

    /**
     * @Description: 获取用户的账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.GET)
    Result getUserRecords(){
        return userService.getUserRecords();
    }

    /**
     * @Description: 通过用户名过获取账户记录
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/account",method = RequestMethod.POST)
    Result getUserRecordsByCondition(@RequestParam("keyword") String keyword){
        return userService.getUserRecordsByCondition(keyword);
    }

    /**
     * @Description: 管理员给用户退款
     * @Author: 陈聪敏
     * @Date: 2020/3/16 0016 18:47
     */
    @RequestMapping(value = "users/refund",method = RequestMethod.PUT)
    public Result refund(@RequestParam("uid") Integer uid,@RequestParam("money")BigDecimal money){
        return userService.refund(uid,money);
    }

    /**
     * @Description: 获取用户的收藏
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/collections",method = RequestMethod.GET)
    Result getUserCollections(@RequestHeader("auth-token") String token){
        return userService.getUserCollections(token);
    }

    /**
     * @Description: 获取用户的浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/browses",method = RequestMethod.GET)
    Result getUserBrowse(@RequestHeader("auth-token") String token){
        return userService.getUserBrowse(token);
    }

    /**
     * @Description: 根据用户id和商品id删除浏览记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/browses/{id}",method = RequestMethod.DELETE)
    Result delUserBrowse(@RequestHeader("auth-token") String token,@RequestParam("id")Integer id){
        return userService.delUserBrowse(token,id);
    }


    /**
     * @Description: 获取用户的评价记录
     * @Author: 孙玮立
     * @Date: 2020/3/20  21:47
     */
    @RequestMapping(value = "users/evaluates",method = RequestMethod.GET)
    Result getUserEvaluate(@RequestHeader("auth-token")String token){
        return userService.getUserEvaluate(token);
    }
}
