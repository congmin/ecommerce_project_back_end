package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.entity.UserQuestion;
import com.sy.mapper.UserQuestionMapper;
import org.springframework.stereotype.Service;

@Service
public class UserQuestionServiceImpl extends ServiceImpl<UserQuestionMapper,UserQuestion> {
}
