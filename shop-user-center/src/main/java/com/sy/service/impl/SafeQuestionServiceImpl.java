package com.sy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.dto.Result;
import com.sy.entity.SafeQuestion;
import com.sy.exception.ShopSystemException;
import com.sy.mapper.SafeQuestionMapper;
import com.sy.service.SafeQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SafeQuestionServiceImpl extends ServiceImpl<SafeQuestionMapper, SafeQuestion> implements SafeQuestionService {

    @Autowired
    private SafeQuestionMapper safeQuestionMapper;

    /**
     * 获得所有安全问题
     * @author 董星奇
     */
    @Override
    public Result allQuestions() {
        Result result = new Result();
        try {
            QueryWrapper<SafeQuestion> safeQuestionQueryWrapper=new QueryWrapper<SafeQuestion>();
            safeQuestionQueryWrapper.isNotNull("id");
            List<SafeQuestion> safeQuestionList=safeQuestionMapper.selectList(safeQuestionQueryWrapper);
            result.setData(safeQuestionList);
            result.setCode(200);
        } catch (ShopSystemException e) {
            e.printStackTrace();
            result.setCode(500);
            result.setError(e.getMessage());
        }

        return result;
    }
}
