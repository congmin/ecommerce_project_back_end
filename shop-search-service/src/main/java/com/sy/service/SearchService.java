package com.sy.service;

import com.sy.dto.Result;
import com.sy.index.BrandIndex;
import com.sy.index.CategoryIndex;
import com.sy.index.ProductIndex;

import java.util.List;


/**
 * @Description:专门操作整个项目的实体类的
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */

public interface SearchService {
    Result getGoodListByNameFromElasticsearch (String goodname);
    Integer deleteByPid(Integer pid);
    ProductIndex findByPid(Integer pid);
    Integer deleteByCid(Integer cid);
    CategoryIndex findByCid(Integer cid);
    Integer deleteByBid(Integer bid);
    BrandIndex findByBid(Integer bid);
    Object save(Object object);
    List<ProductIndex> findAllByBid(Integer bid);
    List<ProductIndex> findAllByCid(Integer cid);
    Integer deleteProductByBid(Integer bid);

}
