package com.sy.service;

import com.sy.entity.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description:这是一个处理秒杀的服务
 * @Param:
 * @return:
 * @Author: 朱天宇
 * @Date:
 */
@FeignClient("seckkill-module")
public interface SeckKillService {
    @RequestMapping(value = "products/good", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
     List<Product> getProductsCanKill ();
}
